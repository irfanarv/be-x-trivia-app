require("dotenv").config();
var express = require("express");
var cors = require("cors");
var app = express();
var port = process.env.PORT;
const helmet = require("helmet");
const bodyParser = require("body-parser");
const logger = require("morgan");

app.use(logger("dev"));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
global.__basedir = __dirname;

//Cors
var corsOption = {
  origin: "*",
  methods: "GET,HEAD,PUT,PATCH,POST,DELETE",
  credentials: true,
  exposedHeaders: ["x-auth-token"],
};

app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept,  Authorization, x-access-token, x-api-key"
  );
  next();
});

// routes
var memberRoutes = require("./app/routes/member.routes.js");
var promotionRoutes = require("./app/routes/promotions.routes.js");
var triviaRoutes = require("./app/routes/trivia.routes.js");
var prizeRoutes = require("./app/routes/prizes.routes.js");
var infoRoutes = require("./app/routes/info.routes.js");
var inboxRoutes = require("./app/routes/inbox.routes.js");
var leadRoutes = require("./app/routes/leaderboard.routes.js");
var referralRoutes = require("./app/routes/referral.routes.js");
var topupRoutes = require("./app/routes/topup.routes.js");
var tutorialRoutes = require("./app/routes/tutorial.routes.js");
var spinRoutes = require("./app/routes/spin.routes.js");
var dailyLogin = require("./app/routes/dailyLogins.routes.js");
var home = require("./app/routes/home.routes.js");

app.use("/member", memberRoutes);
app.use("/promotions", promotionRoutes);
app.use("/trivia", triviaRoutes);
app.use("/info", infoRoutes);
app.use("/inbox", inboxRoutes);
app.use("/leaderboard", leadRoutes);
app.use("/referral-code", referralRoutes);
app.use(topupRoutes);
app.use("/tutorial", tutorialRoutes);
app.use(prizeRoutes);
app.use("/lucky-spin", spinRoutes);
app.use("/daily-login", dailyLogin);
app.use("/home", home);

app.use((req, res) =>
  res.status(404).json({
    status: 404,
    message: "Request not found",
  })
);

app.use(cors(corsOption));
app.use(helmet());

// Listening
app.get("/", function (req, res) {
  console.log("route / is accessed.");
  res.send("API X-TRIVIA");
});

app.listen(port);

module.exports = app;
