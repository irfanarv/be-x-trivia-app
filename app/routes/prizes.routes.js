const express = require("express");
const router = express.Router();
const verifyToken = require("../middleware/verifyToken.middleware");
const verifyAPI = require("../middleware/verifyKey.middleware");
const prizes = require("../api").prizes;

router.get("/prizes", verifyAPI, verifyToken, prizes.main);

router.post("/prize/redeem", verifyAPI, verifyToken, prizes.redeem);

module.exports = router;
