const express = require("express");
const router = express.Router();
const verifyToken = require("../middleware/verifyToken.middleware");
const verifyAPI = require("../middleware/verifyKey.middleware");
const inbox = require("../api").inbox;
const read = require("../api").readMsg;

router.get("/", verifyAPI, verifyToken, inbox.main);
router.post("/update", verifyAPI, verifyToken, read.main);

module.exports = router;
