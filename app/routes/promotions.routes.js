const express = require('express');
const router = express.Router();
const verifyToken = require('../middleware/verifyToken.middleware');
const verifyAPI = require('../middleware/verifyKey.middleware')
const promotions = require("../api").promotions;

router.get(
    "/",
    verifyAPI,
    verifyToken,
    promotions.main
);

module.exports = router