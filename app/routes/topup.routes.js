const express = require("express");
const router = express.Router();
const verifyToken = require("../middleware/verifyToken.middleware");
const verifyAPI = require("../middleware/verifyKey.middleware");
const members = require("../middleware/members.middleware");
const package = require("../api").packageTopup;
const getTransactionId = require("../api").transaction;
const updateTransaction = require("../api").updateTransaction;

router.get("/topup/packages", verifyAPI, verifyToken, package.main);
router.post(
  "/getTransactionId/",
  verifyAPI,
  verifyToken,
  members.cekMemberParam,
  members.fieldTopupPhone,
  members.fieldTopupCredit,
  getTransactionId.main
);
router.post(
  "/updateTransaction/",
  verifyAPI,
  verifyToken,
  members.fieldUpdateTrx,
  members.checkTrxId,
  updateTransaction.main
);

module.exports = router;
