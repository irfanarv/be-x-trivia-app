const express = require("express");
const router = express.Router();
const verifyToken = require("../middleware/verifyToken.middleware");
const verifyAPI = require("../middleware/verifyKey.middleware");
const home = require("../api").homeCheck;

router.get("/", verifyAPI, verifyToken, home.main);

module.exports = router;
