const express = require("express");
const router = express.Router();
const verifyToken = require("../middleware/verifyToken.middleware");
const verifyAPI = require("../middleware/verifyKey.middleware");
const triviaMiddleware = require("../middleware/trivia.middleware");
const categoryTrivia = require("../api").categoryTrivia;
const subCategoryTrivia = require("../api").subCategoryTrivia;
const question = require("../api").question;
const answer = require("../api").answer;

router.get("/categories", verifyAPI, verifyToken, categoryTrivia.main);

router.get(
  "/sub-categories/",
  verifyAPI,
  verifyToken,
  subCategoryTrivia.getByid
);

router.get(
  "/questions/",
  verifyAPI,
  verifyToken,
  triviaMiddleware.subCategoryCheck,
  triviaMiddleware.checkCredits,
  question.main
);

router.post(
  "/answer",
  verifyAPI,
  verifyToken,
  triviaMiddleware.checkAnswer,
  answer.main
);

module.exports = router;
