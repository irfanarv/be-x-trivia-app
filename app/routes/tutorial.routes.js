const express = require("express");
const router = express.Router();
const verifyToken = require("../middleware/verifyToken.middleware");
const verifyAPI = require("../middleware/verifyKey.middleware");
const tutorial = require("../api").tutorial;

router.get("/home", verifyAPI, verifyToken, tutorial.home);
router.get("/trivia", verifyAPI, verifyToken, tutorial.trivia);
router.post("/home/finish", verifyAPI, verifyToken, tutorial.finish);

module.exports = router;
