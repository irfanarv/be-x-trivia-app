const express = require("express");
const router = express.Router();
const verifyAPI = require("../middleware/verifyKey.middleware");
const verifyToken = require("../middleware/verifyToken.middleware");
const checkSMSApi = require("../middleware/checkSMSApi.middleware");
const members = require("../middleware/members.middleware");
const signup = require("../api").signup;
const validateOTP = require("../api").validateOtp;
const login = require("../api").login;
const profile = require("../api").profile;
const unsubscribe = require("../api").unsubscribe;
const changeAva = require("../api").updateAva;

router.post(
  "/signup",
  verifyAPI,
  checkSMSApi,
  members.fieldPhone,
  members.memberRegister,
  signup.main
);
router.post(
  "/signup/validate-otp",
  verifyAPI,
  checkSMSApi,
  members.fieldPhone,
  members.memberLogin,
  members.fieldOTP,
  validateOTP.main
);
router.post(
  "/signup/validate-referral",
  verifyAPI,
  checkSMSApi,
  members.fieldReferral,
  members.chekReferral
);
router.post(
  "/login",
  verifyAPI,
  checkSMSApi,
  members.fieldPhone,
  members.memberLogin,
  members.memberStatus,
  login.main
);
router.post(
  "/login/validate-otp",
  verifyAPI,
  checkSMSApi,
  members.fieldOTP,
  validateOTP.mainLogin
);

router.get("/profile", verifyAPI, verifyToken, profile.main);
router.post(
  "/unsubscribe",
  verifyAPI,
  verifyToken,
  members.memberUnsbuscribe,
  unsubscribe.main
);

router.post("/update-pp", verifyAPI, verifyToken, changeAva.change);

router.get("/profile/:name", profile.getFile);
module.exports = router;
