const express = require("express");
const router = express.Router();
const verifyToken = require("../middleware/verifyToken.middleware");
const verifyAPI = require("../middleware/verifyKey.middleware");
const referral = require("../api").referral;

router.get("/", verifyAPI, verifyToken, referral.main);

module.exports = router;
