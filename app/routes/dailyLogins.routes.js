const express = require("express");
const router = express.Router();
const verifyToken = require("../middleware/verifyToken.middleware");
const verifyAPI = require("../middleware/verifyKey.middleware");
const dailyLogin = require("../api").dailyLogin;
const claimLogin = require("../api").claimLogin;

router.get("/", verifyAPI, verifyToken, dailyLogin.main);
router.post("/claim", verifyAPI, verifyToken, claimLogin.main);

module.exports = router;
