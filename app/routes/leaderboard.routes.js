const express = require("express");
const router = express.Router();
const verifyToken = require("../middleware/verifyToken.middleware");
const verifyAPI = require("../middleware/verifyKey.middleware");
const leaderboard = require("../api").leaderboard;

router.get("/", verifyAPI, verifyToken, leaderboard.main);

module.exports = router;
