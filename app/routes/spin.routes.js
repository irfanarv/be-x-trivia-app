const express = require("express");
const router = express.Router();
const verifyToken = require("../middleware/verifyToken.middleware");
const verifyAPI = require("../middleware/verifyKey.middleware");
const spinKuota = require("../middleware/trivia.middleware");
const spin = require("../api").spin;
const spinPlay = require("../api").spinPlay;

router.get("/prizes", verifyAPI, verifyToken, spinKuota.spinKuota, spin.main);
router.post("/", verifyAPI, verifyToken, spinKuota.spinKuota, spinPlay.main);

module.exports = router;
