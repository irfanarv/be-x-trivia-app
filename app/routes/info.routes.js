const express = require("express");
const router = express.Router();
const verifyToken = require("../middleware/verifyToken.middleware");
const verifyAPI = require("../middleware/verifyKey.middleware");
const info = require("../api").info;

router.get("/tnc", verifyAPI, verifyToken, info.getTnC);

router.get("/privacy-policy", verifyAPI, verifyToken, info.getPrivacy);
router.get("/about", verifyAPI, verifyToken, info.getAbout);

module.exports = router;
