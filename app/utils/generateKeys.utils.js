const res = require('express/lib/response');
var otpGenerator = require('otp-generator')
var randomstring = require("randomstring")


module.exports.generateKeyEnv = function () {
    let r = Math.random().toString(36).substr(2, 6) + "-" + Math.random().toString(36).substr(2, 6) + "-" + Math.random().toString(36).substr(2, 4);
    console.log(r.toUpperCase());
};

module.exports.referral = function () {
    return otpGenerator.generate(6, { upperCaseAlphabets: false, specialChars: false });
};

module.exports.otp = function () {
    return Math.floor(1000 + Math.random() * 9000);
};

module.exports.keyApi = function () {
    var getKey =  randomstring.generate();
    console.log(getKey);
};
