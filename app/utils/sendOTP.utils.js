const axios = require('axios')

const sendSMS = (OTPCode,msisdn) =>{
    return axios.post(process.env.GOSMS_API + 'username=' + process.env.GOSMS_USER + '&mobile=' + msisdn + '&message=' + OTPCode + '&password=' + process.env.GOSMS_PWD);
}

module.exports = sendSMS;