const spinPlay = require("../../models").lucky_spin_plays;
const jwt = require("jsonwebtoken");
const secret = process.env.SECRET;

module.exports = {
  async main(req, res) {
    try {
      const now = new Date();
      const usertoken = req.headers["x-access-token"];
      const token = usertoken.split(" ");
      const userData = jwt.verify(token[1], secret);
      const idMember = userData.member_id;
      const { type, point } = req.body;
      await spinPlay
        .create({
          member_id: idMember,
          date: now,
          type: type,
          point: point,
        })
        .then((result) => {
          const response = {
            status: 200,
            success: true,
            message: "Success",
            data: {
              results: result.type + " " + result.point,
            },
          };
          return res.status(200).send(response);
        });
    } catch (err) {
      const response = {
        status: 400,
        success: false,
        message: "Bad Request",
        data: {
          errors: err.message,
        },
      };
      return res.status(400).send(response);
    }
  },
};
