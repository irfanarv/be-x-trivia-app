const totalSpins = require("../../models").member_spins;
const spinPlay = require("../../models").lucky_spin_plays;
const dataSpin = require("../../models").lucky_spin_prizes;
const jwt = require("jsonwebtoken");
const secret = process.env.SECRET;

module.exports = {
  async main(req, res) {
    try {
      const usertoken = req.headers["x-access-token"];
      const token = usertoken.split(" ");
      const userData = jwt.verify(token[1], secret);
      const idMember = userData.member_id;
      const getKuotaSpin = await totalSpins.sum("quota_spin", {
        where: {
          member_id: idMember,
        },
      });
      const getTotalPlaySpin = await spinPlay.count({
        where: {
          member_id: idMember,
        },
      });
      if (!getTotalPlaySpin) {
        var spinTotals = getKuotaSpin;
      } else {
        var spinTotals = getKuotaSpin - getTotalPlaySpin;
      }
      await dataSpin
        .findAll({
          attributes: ["id", "sort_no", "type", "point", "label", "bg_color"],
          order: [["sort_no", "ASC"]],
        })
        .then((results) => {
          const spinData = [];
          results.forEach((results) => {
            spinData.push({
              id: results.id,
              sortNo: results.sort_no,
              type: results.type,
              point: results.point,
              label: results.label,
              colorBG: results.bg_color,
            });
          });

          const response = {
            status: 200,
            success: true,
            message: "OK",
            data: {
              spinTotals: spinTotals,
              spinData,
            },
          };
          return res.status(200).send(response);
        });
    } catch (err) {
      const response = {
        status: 400,
        success: false,
        message: "Bad Request",
        data: {
          errors: err.message,
        },
      };
      return res.status(400).send(response);
    }
  },
};
