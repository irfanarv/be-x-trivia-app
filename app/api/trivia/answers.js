const questions = require("../../models").trivia_questions;
const triviaAnswer = require("../../models").trivia_answers;
const questionDetails = require("../../models").trivia_question_details;
const jwt = require("jsonwebtoken");
const secret = process.env.SECRET;

module.exports = {
  async main(req, res) {
    try {
      const usertoken = req.headers["x-access-token"];
      const token = usertoken.split(" ");
      const userData = jwt.verify(token[1], secret);
      const idMember = userData.member_id;
      const { questionID, answerID } = req.body;

      const getSubCategoryId = await questions.findOne({
        attributes: ["trivia_sub_category_id"],
        where: {
          id: questionID,
        },
      });
      subCategoryId = getSubCategoryId.trivia_sub_category_id;

      if (parseInt(answerID) === 0) {
        await triviaAnswer
          .create({
            member_id: idMember,
            trivia_sub_category_id: subCategoryId,
            trivia_question_id: questionID,
            trivia_question_detail_id: answerID,
            is_correct: 2,
            point: 0,
          })
          .then((results) => {
            const response = {
              status: 200,
              success: false,
              message: "Waktu Habis",
            };
            return res.status(200).send(response);
          });
      } else {
        await questionDetails
          .findOne({
            where: {
              id: answerID,
            },
          })
          .then(async (postAnswer) => {
            if (postAnswer.is_correct === 0) {
              var jawaban = 0;
              var point = 0;
            } else if (postAnswer.is_correct === 1) {
              var jawaban = 1;
              var point = 10;
            }
            await triviaAnswer
              .create({
                member_id: idMember,
                trivia_sub_category_id: subCategoryId,
                trivia_question_id: questionID,
                trivia_question_detail_id: answerID,
                is_correct: jawaban,
                point: point,
              })
              .then((results) => {
                const response = {
                  status: 200,
                  success: true,
                  message: "Submit telah berhasil",
                  data: {
                    idAnswer: results.id,
                    idMember: results.member_id,
                    idQuestion: results.trivia_question_id,
                    idQuestionDetail: results.trivia_question_detail_id,
                    isCorrect: results.is_correct,
                  },
                };
                return res.status(200).send(response);
              });
          });
      }
    } catch (err) {
      const response = {
        status: 400,
        success: false,
        message: "Bad Request",
        data: {
          errors: err.message,
        },
      };
      return res.status(400).send(response);
    }
  },
};
