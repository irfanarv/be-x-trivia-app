const subCategory = require('../../models').trivia_sub_categories
const Categories = require('../../models').trivia_categories

module.exports = {
  async main(req, res) {
    try {
      var getSubCategories = await subCategory.findAll({
        include: [{
          model: Categories,
          as: 'trivia_category'
        }],
        order: [
          ['createdAt', 'ASC']
        ],
      });

      const subCategories = [];
      getSubCategories.forEach((getSubCategories) => {
        subCategories.push({
          categoryName: getSubCategories.trivia_category.name,
          subCategoryName: getSubCategories.name,
          subCategoryImage: process.env.URL_FILE + '/trivia/sub-category/' + getSubCategories.image,
        });
      });
      if (!subCategories.length) {
        const response = {
          "status": 200,
          "success": true,
          "subCategoryStatus": null,
          "message": "Sub Kategori trivia tidak tersedia",
        }
        return res.status(200).send(response)
      } else {
        const response = {
          "status": 200,
          "success": true,
          "subCategoryStatus": true,
          "message": "Sub Kategori trivia tersedia",
          "data": subCategories
        }
        return res.status(200).send(response)
      }


    } catch (err) {
      const response = {
        "status": 400,
        "success": false,
        "message": "Bad Request",
        "data": {
          "errors": err.message
        }
      }
      return res.status(400).send(response)
    }
  },

  async getByid(req, res) {
    try {
      var categoryID = req.query.category_id
      var getSubCategories = await subCategory.findAll(
        {
          where: {
            trivia_category_id: categoryID
          },
          include: [{
            model: Categories,
            as: 'trivia_category'
          }],
          order: [
            ['createdAt', 'ASC']
          ],
        }
      );

      const subCategories = [];
      getSubCategories.forEach((getSubCategories) => {
        subCategories.push({
          category: getSubCategories.trivia_category.name,
          subCategoryId: getSubCategories.id,
          subCategoryName: getSubCategories.name,
          subCategoryImage: process.env.URL_FILE + '/trivia/sub-category/' + getSubCategories.image,
        });
      });
      if (!subCategories) {
        const response = {
          "status": 200,
          "success": true,
          "subCategoryStatus": null,
          "message": "Sub Kategori trivia tidak tersedia",
        }
        return res.status(200).send(response)
      } else {
        const response = {
          "status": 200,
          "success": true,
          "subCategoryStatus": true,
          "message": "Sub Kategori trivia tersedia",
          "data": subCategories
        }
        return res.status(200).send(response)
      }


    } catch (err) {
      const response = {
        "status": 400,
        "success": false,
        "message": "Bad Request",
        "data": {
          "errors": err.message
        }
      }
      return res.status(400).send(response)
    }
  },
}