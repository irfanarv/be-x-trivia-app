const Categories = require("../../models").trivia_categories;

module.exports = {
  async main(req, res) {
    try {
      var getCategories = await Categories.findAll({
        order: [["createdAt", "ASC"]],
      });

      const categories = [];
      getCategories.forEach((getCategories) => {
        categories.push({
          categoryId: getCategories.id,
          categoryName: getCategories.name,
          categoryImage:
            process.env.URL_FILE + "/trivia/category/" + getCategories.image,
          categoryImageActive:
            process.env.URL_FILE +
            "/trivia/category/" +
            getCategories.image_active,
        });
      });
      if (!categories.length) {
        const response = {
          status: 200,
          success: true,
          categoryStatus: null,
          message: "Kategori trivia tidak tersedia",
        };
        return res.status(200).send(response);
      } else {
        const response = {
          status: 200,
          success: true,
          categoryStatus: true,
          message: "Kategori trivia tersedia",
          data: categories,
        };
        return res.status(200).send(response);
      }
    } catch (err) {
      const response = {
        status: 400,
        success: false,
        message: "Bad Request",
        data: {
          errors: err.message,
        },
      };
      return res.status(400).send(response);
    }
  },
};
