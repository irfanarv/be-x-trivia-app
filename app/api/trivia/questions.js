const questions = require("../../models").trivia_questions;
const triviaAnswer = require("../../models").trivia_answers;
const totalCredits = require("../../models").member_credits;
const prizeRedeem = require("../../models").prize_redeems;
const spinPlay = require("../../models").lucky_spin_plays;
const questionDetails = require("../../models").trivia_question_details;
const jwt = require("jsonwebtoken");
const secret = process.env.SECRET;
const { Op } = require("sequelize");

module.exports = {
  async main(req, res) {
    try {
      const usertoken = req.headers["x-access-token"];
      const token = usertoken.split(" ");
      const userData = jwt.verify(token[1], secret);
      const idMember = userData.member_id;
      // get credits
      var getCredits = await totalCredits.sum("amount", {
        where: {
          member_id: idMember,
        },
      });

      var getAnswerTotal = await triviaAnswer.count({
        where: {
          member_id: idMember,
        },
      });
      if (!getAnswerTotal) {
        var creditTotals = getCredits;
      } else {
        var creditTotals = getCredits - getAnswerTotal;
      }
      // end get credits
      // get point
      var getPoints = await triviaAnswer.sum("point", {
        where: {
          member_id: idMember,
        },
      });
      var getPointFromPrize = await prizeRedeem.sum("redeem_point", {
        where: {
          member_id: idMember,
          result: 1,
        },
      });
      var poinFromSpin = await spinPlay.sum("point", {
        where: {
          member_id: idMember,
          type: "point",
        },
      });

      if (!getPointFromPrize && !poinFromSpin) {
        var pointTotals = getPoints;
      } else if (!poinFromSpin) {
        var pointTotals = getPoints - getPointFromPrize;
      } else if (!getPointFromPrize) {
        var pointTotals = getPoints + poinFromSpin;
      } else {
        var pointTotals = poinFromSpin + getPoints - getPointFromPrize;
      }
      // end get point
      await triviaAnswer
        .findOne({
          where: {
            member_id: idMember,
            trivia_sub_category_id: req.query.sub_category_id,
          },
          order: [["trivia_question_id", "DESC"]],
        })
        .then(async (results) => {
          if (results) {
            await questions
              .findOne({
                where: {
                  trivia_sub_category_id: req.query.sub_category_id,
                  id: {
                    [Op.gt]: results.trivia_question_id,
                  },
                },
                include: [
                  {
                    model: questionDetails,
                    as: "trivia_question_details",
                  },
                ],
              })
              .then((resultQuestion) => {
                if (!resultQuestion) {
                  const response = {
                    status: 200,
                    success: false,
                    message:
                      "Kamu sudah menjawab semua pertanyaan di trivia ini!",
                    data: null,
                  };
                  return res.status(200).send(response);
                }
                if (resultQuestion.id === results.trivia_question_id) {
                  const response = {
                    status: 200,
                    success: false,
                    message:
                      "Kamu sudah menjawab semua pertanyaan di trivia ini!",
                    data: null,
                  };
                  return res.status(200).send(response);
                } else {
                  if (resultQuestion.image === null) {
                    var imgQuestion = null;
                  } else {
                    var imgQuestion =
                      process.env.URL_FILE +
                      "/trivia/question-assets/" +
                      resultQuestion.image;
                  }
                  const answers = resultQuestion.trivia_question_details;
                  const answer = [];
                  answers.forEach((answers) => {
                    answer.push({
                      answerID: answers.id,
                      answerText: answers.answer_text,
                      isCorrect: answers.is_correct,
                    });
                  });
                  const response = {
                    status: 200,
                    success: true,
                    message: "OK",
                    data: {
                      yourPoint: pointTotals,
                      yourCredits: creditTotals,
                      questionID: resultQuestion.id,
                      question: resultQuestion.title,
                      questionImg: imgQuestion,
                      answer: answer,
                    },
                  };
                  return res.status(200).send(response);
                }
              });
          } else if (!results) {
            await questions
              .findOne({
                where: {
                  trivia_sub_category_id: req.query.sub_category_id,
                },
                include: [
                  {
                    model: questionDetails,
                    as: "trivia_question_details",
                  },
                ],
              })
              .then((resultQuestion) => {
                if (!resultQuestion) {
                  const response = {
                    status: 200,
                    success: true,
                    message: "Pertanyaan kosong!",
                    data: null,
                  };
                  return res.status(200).send(response);
                } else {
                  if (resultQuestion.image === null) {
                    var imgQuestion = null;
                  } else {
                    var imgQuestion =
                      process.env.URL_FILE +
                      "/trivia/question-assets/" +
                      resultQuestion.image;
                  }
                  const answers = resultQuestion.trivia_question_details;
                  const answer = [];
                  answers.forEach((answers) => {
                    answer.push({
                      answerID: answers.id,
                      answerText: answers.answer_text,
                      isCorrect: answers.is_correct,
                    });
                  });
                  const response = {
                    status: 200,
                    success: true,
                    message: "OK",
                    data: {
                      yourPoint: pointTotals,
                      yourCredits: creditTotals,
                      questionID: resultQuestion.id,
                      question: resultQuestion.title,
                      questionImg: imgQuestion,
                      answer: answer,
                    },
                  };
                  return res.status(200).send(response);
                }
              });
          }
        });
    } catch (err) {
      const response = {
        status: 400,
        success: false,
        message: "Bad Request",
        data: {
          errors: err.message,
        },
      };
      return res.status(400).send(response);
    }
  },
};
