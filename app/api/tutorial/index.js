const tutorial = require("../../models").tutorials;
const categories = require("../../models").tutorial_categories;
const member = require("../../models").members;
const jwt = require("jsonwebtoken");
const secret = process.env.SECRET;

module.exports = {
  async home(req, res) {
    try {
      const usertoken = req.headers["x-access-token"];
      const token = usertoken.split(" ");
      const userData = jwt.verify(token[1], secret);
      const idMember = userData.member_id;

      await member
        .findOne({
          where: {
            id: idMember,
          },
        })
        .then(async (results) => {
          if (results.home_tutorial === 1) {
            const response = {
              statusTutorial: 0,
              status: 400,
              success: false,
              message: "Tutorial home passed",
            };
            return res.status(400).send(response);
          }
          await tutorial
            .findAll({
              where: {
                tutorial_category_id: 1,
              },
              include: [
                {
                  model: categories,
                  as: "tutorial_category",
                },
              ],
              order: [["step", "ASC"]],
            })
            .then(async (resultTutorial) => {
              const listStep = [];
              resultTutorial.forEach((resultTutorial) => {
                listStep.push({
                  category: resultTutorial.tutorial_category.title,
                  step: resultTutorial.step,
                  image:
                    process.env.URL_FILE + "/tutorial/" + resultTutorial.image,
                });
              });
              const response = {
                statusTutorial: 1,
                status: 200,
                success: true,
                message: "OK",
                data: listStep,
              };
              return res.status(200).send(response);
            });
        });
    } catch (err) {
      const response = {
        status: 400,
        success: false,
        message: "Bad Request",
        data: {
          errors: err.message,
        },
      };
      return res.status(400).send(response);
    }
  },
  async trivia(req, res) {
    try {
      const usertoken = req.headers["x-access-token"];
      const token = usertoken.split(" ");
      const userData = jwt.verify(token[1], secret);
      const idMember = userData.member_id;

      await member
        .findOne({
          where: {
            id: idMember,
          },
        })
        .then(async (results) => {
          await tutorial
            .findAll({
              where: {
                tutorial_category_id: 2,
              },
              include: [
                {
                  model: categories,
                  as: "tutorial_category",
                },
              ],
              order: [["step", "ASC"]],
            })
            .then(async (resultTutorial) => {
              const listStep = [];
              resultTutorial.forEach((resultTutorial) => {
                listStep.push({
                  category: resultTutorial.tutorial_category.title,
                  step: resultTutorial.step,
                  image:
                    process.env.URL_FILE + "/tutorial/" + resultTutorial.image,
                });
              });
              const response = {
                status: 200,
                success: true,
                message: "OK",
                data: listStep,
              };
              return res.status(200).send(response);
            });
        });
    } catch (err) {
      const response = {
        status: 400,
        success: false,
        message: "Bad Request",
        data: {
          errors: err.message,
        },
      };
      return res.status(400).send(response);
    }
  },
  async finish(req, res) {
    try {
      const usertoken = req.headers["x-access-token"];
      const token = usertoken.split(" ");
      const userData = jwt.verify(token[1], secret);
      const idMember = userData.member_id;

      await member
        .findOne({
          where: {
            id: idMember,
          },
        })
        .then(async (results) => {
          if (results.home_tutorial === 1) {
            const response = {
              status: 400,
              success: false,
              message: "Anda sudah menyelesaikan tutorial home!",
            };
            return res.status(400).send(response);
          } else {
            await member
              .update(
                {
                  home_tutorial: 1,
                },
                {
                  where: {
                    id: idMember,
                  },
                }
              )
              .then(async (resultUpdate) => {
                const response = {
                  status: 200,
                  success: true,
                  message: "Berhasil menyelesaikan tutorial!",
                };
                return res.status(200).send(response);
              });
          }
        });
    } catch (err) {
      const response = {
        status: 400,
        success: false,
        message: "Bad Request",
        data: {
          errors: err.message,
        },
      };
      return res.status(400).send(response);
    }
  },
};
