const member = require("../../models").members;
const triviaAnswer = require("../../models").trivia_answers;
const prizeRedeem = require("../../models").prize_redeems;
const spinPlay = require("../../models").lucky_spin_plays;
const sequelize = require("sequelize");

module.exports = {
  async main(req, res) {
    try {
      var poinFromSpin = await spinPlay.findAll({
        attributes: [
          "member_id",
          [sequelize.fn("sum", sequelize.col("point")), "point"],
        ],
        group: ["member_id"],
        raw: true,
        where: {
          type: "point",
        },
      });
      var pointAnswer = await triviaAnswer.findAll({
        attributes: [
          "member_id",
          [sequelize.fn("sum", sequelize.col("point")), "point"],
        ],
        group: ["member_id"],
        include: [
          {
            model: member,
            as: "member",
            attributes: ["msisdn", "photo"],
          },
        ],
        limit: 10,
      });
      var totalPointAnswer = [];
      pointAnswer.forEach((pointAnswer) => {
        if (pointAnswer.member.photo === null) {
          var imgUser =
            process.env.URL_PROFILE + "/member/profile/" + "profile.png";
        } else {
          var imgUser =
            process.env.URL_PROFILE +
            "/member/profile/" +
            pointAnswer.member.photo;
        }
        var phoneNumber =
          pointAnswer.member.msisdn.slice(0, 2) +
          pointAnswer.member.msisdn.slice(2).replace(/.(?=...)/g, "*");

        totalPointAnswer.push({
          member_id: pointAnswer.member_id,
          photo: imgUser,
          phone: phoneNumber,
          point: pointAnswer.point,
        });
      });

      var result = totalPointAnswer.concat(poinFromSpin).reduce(
        function (acc, curr) {
          if (curr.member_id) {
            var fromMap = acc.map[curr.member_id];
            if (!fromMap) {
              acc.map[curr.member_id] = fromMap = {
                member_id: curr.member_id,
                photo: curr.photo,
                phone: curr.phone,
                point: 0,
              };
              acc.result.push(fromMap);
            }
            fromMap.point += parseFloat(curr.point);
          } else {
            acc.result.push(curr);
          }
          return acc;
        },
        {
          map: {},
          result: [],
        }
      ).result;

      var pointRedeem = await prizeRedeem.findAll({
        attributes: [
          "member_id",
          [sequelize.fn("sum", sequelize.col("redeem_point")), "point"],
        ],
        group: ["member_id"],
        raw: true,
        where: {
          result: 1,
        },
      });

      function leaderboard(arrayOfArrays, propToCheck, propToSum) {
        let sum = [];
        [].concat(...arrayOfArrays).map(function (o) {
          let existing = sum.filter(function (i) {
            return i[propToCheck] === o[propToCheck];
          })[0];

          if (!existing) {
            sum.push(o);
          } else {
            existing[propToSum] -= o[propToSum];

            let copyProps = Object.keys(o)
              .filter((obj) => {
                return existing[obj] !== o[obj];
              })
              .map((val) =>
                val !== propToSum ? (existing[val] = o[val]) : null
              );
          }
        });
        return sum;
      }

      const response = {
        status: 200,
        success: true,
        message: "OK",
        data: leaderboard(
          [result, pointRedeem],
          "member_id",
          "point",
          "number"
        ).sort((a, b) => parseFloat(b.point) - parseFloat(a.point)),
      };
      return res.status(200).send(response);
    } catch (err) {
      const response = {
        status: 400,
        success: false,
        message: "Bad Request",
        data: {
          errors: err.message,
        },
      };
      return res.status(400).send(response);
    }
  },
};
