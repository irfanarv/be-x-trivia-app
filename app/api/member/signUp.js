const Members = require("../../models").members;
const otpModels = require("../../models").otp;
const dates = require("../../utils/dates.util");
const generate = require("../../utils/generateKeys.utils");
const messageTemplate = require("../../templates/sms/sendOTP");
const sendMessage = require("../../utils/sendOTP.utils");

module.exports = {
  async main(req, res) {
    try {
      const referralCode = generate.referral();
      // const otp = generate.otp();
      const otp = 1945;
      const now = new Date();
      const { msisdn, reg_referral_id } = req.body;
      const expiration_time = dates.addMinutes(now, 5);

      await Members.findOne({
        where: {
          referral_id: reg_referral_id,
        },
      }).then(async (referralCheck) => {
        if (referralCheck) {
          await Members.create({
            msisdn: msisdn,
            referral_id: referralCode,
            reg_referral_id: reg_referral_id,
          }).then((result) => (lastID = result.id));
          await otpModels.create({
            member_id: lastID,
            otp: otp,
            type: 1,
            expiration_time: expiration_time,
          });
        } else {
          await Members.create({
            msisdn: msisdn,
            referral_id: referralCode,
          }).then((result) => (lastID = result.id));
          await otpModels.create({
            member_id: lastID,
            otp: otp,
            type: 1,
            expiration_time: expiration_time,
          });
        }
        // OTPCode = messageTemplate(otp);
        // sendMessage(OTPCode, msisdn);
        const response = {
          status: 200,
          success: true,
          message: "Kode OTP berhasil dikirim",
          data: {
            phoneNumber: req.body.msisdn,
            otp: otp,
            typeOTP: 1,
            waktuKirim: now,
          },
        };
        return res.status(200).send(response);
      });
    } catch (err) {
      const response = {
        status: 400,
        success: false,
        message: "Bad Request",
        data: {
          errors: err.message,
        },
      };
      return res.status(400).send(response);
    }
  },
};
