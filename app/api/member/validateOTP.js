const memberLogins = require("../../models").member_logins;
const Members = require("../../models").members;
const otpModels = require("../../models").otp;
const jwt = require("jsonwebtoken");
var dates = require("../../utils/dates.util");
const memberCredits = require("../../models").member_credits;
const inbox = require("../../models").inboxes;
module.exports = {
  async main(req, res) {
    try {
      var currentdate = new Date();
      var getOTP = await otpModels.findOne({
        where: {
          otp: req.body.otp,
          type: 1,
        },
        include: [
          {
            model: Members,
            as: "member",
            where: {
              msisdn: req.body.msisdn,
            },
          },
        ],
        order: [["createdAt", "DESC"]],
      });
      if (getOTP) {
        if (dates.compare(getOTP.expiration_time, currentdate) == 1) {
          const msg =
            "Selamat! Kamu mendapatkan 50 credit pertanyaan, segera mainkan dan dapatkan hadiahnya !";
          const titleInbox = "Hadiah Sign Up";
          await Members.update(
            {
              status: 1,
            },
            {
              where: {
                msisdn: getOTP.member.msisdn,
              },
            }
          );
          await memberLogins.create({
            member_id: getOTP.member_id,
            otp: req.body.otp,
          });
          await memberCredits.create({
            member_id: getOTP.member_id,
            date: currentdate,
            amount: 50,
            type: "Signup",
          });
          await inbox.create({
            member_id: getOTP.member_id,
            title: titleInbox,
            text: msg,
          });
          var token =
            "Bearer " +
            jwt.sign(
              {
                member_id: getOTP.member_id,
                msisdn: getOTP.member.msisdn,
              },
              process.env.SECRET,
              {
                expiresIn: "5d",
              }
            );
          res.status(200).send({
            status: 200,
            success: true,
            message: "Token berhasil dibuat",
            data: {
              id: getOTP.id,
              member: getOTP.member.msisdn,
              accessToken: token,
            },
          });
        } else {
          const response = {
            status: 400,
            success: false,
            message: "Kode OTP Expired",
          };
          return res.status(400).send(response);
        }
      } else {
        res.status(400).send({
          status: 400,
          success: false,
          message: "Kode OTP tidak sesuai !",
        });
        return;
      }
    } catch (err) {
      const response = {
        status: 400,
        success: false,
        message: "Bad Request",
        data: {
          errors: err.message,
        },
      };
      return res.status(400).send(response);
    }
  },

  async mainLogin(req, res) {
    try {
      var currentdate = new Date();
      var getOTP = await otpModels.findOne({
        where: {
          otp: req.body.otp,
          type: 2,
        },
        include: [
          {
            model: Members,
            as: "member",
            where: {
              msisdn: req.body.msisdn,
            },
          },
        ],
        order: [["createdAt", "DESC"]],
      });
      if (getOTP) {
        if (dates.compare(getOTP.expiration_time, currentdate) == 1) {
          await memberLogins.create({
            member_id: getOTP.member_id,
            otp: req.body.otp,
          });
          var token =
            "Bearer " +
            jwt.sign(
              {
                member_id: getOTP.member_id,
                msisdn: getOTP.member.msisdn,
              },
              process.env.SECRET,
              {
                expiresIn: "5d",
              }
            );
          res.status(200).send({
            status: 200,
            success: true,
            message: "Token berhasil dibuat",
            data: {
              id: getOTP.id,
              member: getOTP.member.msisdn,
              accessToken: token,
            },
          });
        } else {
          const response = {
            status: 400,
            success: false,
            message: "Kode OTP Expired",
          };
          return res.status(400).send(response);
        }
      } else {
        res.status(400).send({
          status: 400,
          success: false,
          message: "Kode OTP tidak sesuai !",
        });
        return;
      }
    } catch (err) {
      const response = {
        status: 400,
        success: false,
        message: "Bad Request",
        data: {
          errors: err.message,
        },
      };
      return res.status(400).send(response);
    }
  },
};
