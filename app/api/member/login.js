const Members = require("../../models").members;
const otpModels = require("../../models").otp;
const memberLogins = require("../../models").member_logins;
const dates = require("../../utils/dates.util");
const generate = require("../../utils/generateKeys.utils");
const messageTemplate = require("../../templates/sms/sendOTP");
const sendMessage = require("../../utils/sendOTP.utils");

module.exports = {
  async main(req, res) {
    try {
      const msisdn = req.body.msisdn;
      // const otp = generate.otp();
      const otp = 1945;
      const now = new Date();
      const expiration_time = dates.addMinutes(now, 5);
      const getID = await Members.findOne({
        where: {
          msisdn: msisdn,
        },
      });
      await otpModels.create({
        member_id: getID.id,
        otp: otp,
        type: 2,
        expiration_time: expiration_time,
      });
      // OTPCode = messageTemplate(otp);
      // sendMessage(OTPCode, msisdn);
      const response = {
        status: 200,
        success: true,
        message: "Kode OTP berhasil dikirim",
        data: {
          phoneNumber: msisdn,
          otp: otp,
          typeOTP: 2,
          waktuKirim: now,
        },
      };
      return res.status(200).send(response);
    } catch (err) {
      const response = {
        status: 400,
        success: false,
        message: "Bad Request",
        data: {
          errors: err.message,
        },
      };
      return res.status(400).send(response);
    }
  },
};
