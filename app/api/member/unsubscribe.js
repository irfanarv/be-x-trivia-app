const members = require("../../models").members;
const inbox = require("../../models").inboxes;
const jwt = require("jsonwebtoken");
const secret = process.env.SECRET;

module.exports = {
  async main(req, res) {
    try {
      const usertoken = req.headers["x-access-token"];
      const token = usertoken.split(" ");
      const userData = jwt.verify(token[1], secret);
      const idMember = userData.member_id;
      const msg = "Kamu telah berhenti berlangganan X-Trivia";
      const titleInbox = "Berhenti berlangganan";
      await members
        .update(
          {
            status: 5,
          },
          {
            where: {
              id: idMember,
            },
          }
        )
        .then(async (results) => {
          await inbox.create({
            member_id: idMember,
            title: titleInbox,
            text: msg,
          });
          response = {
            status: 200,
            success: true,
            message: msg,
          };
          return res.status(200).send(response);
        });
    } catch (err) {
      const response = {
        status: 400,
        success: false,
        message: "Bad Request",
        data: {
          errors: err.message,
        },
      };
      return res.status(400).send(response);
    }
  },
};
