const members = require("../../models").members;
const questions = require("../../models").trivia_questions;
const triviaAnswer = require("../../models").trivia_answers;
const totalCredits = require("../../models").member_credits;
const prizeRedeem = require("../../models").prize_redeems;
const questionDetails = require("../../models").trivia_question_details;
const spinPlay = require("../../models").lucky_spin_plays;
const inbox = require("../../models").inboxes;
const jwt = require("jsonwebtoken");
const secret = process.env.SECRET;
const { Op } = require("sequelize");

module.exports = {
  async main(req, res) {
    try {
      const usertoken = req.headers["x-access-token"];
      const token = usertoken.split(" ");
      const userData = jwt.verify(token[1], secret);
      const idMember = userData.member_id;
      const inboxUnread = await inbox.count({
        where: {
          read_status: 0,
          member_id: idMember,
        },
      });

      const profile = await members.findOne({
        where: {
          id: idMember,
        },
      });

      if (profile.photo === null) {
        var imgUser =
          process.env.URL_PROFILE + "/member/profile/" + "profile.png";
      } else {
        var imgUser =
          process.env.URL_PROFILE + "/member/profile/" + profile.photo;
      }

      const getCredits = await totalCredits.sum("amount", {
        where: {
          member_id: idMember,
        },
      });

      const getAnswerTotal = await triviaAnswer.count({
        where: {
          member_id: idMember,
        },
      });
      if (!getAnswerTotal) {
        var creditTotals = getCredits;
      } else {
        var creditTotals = getCredits - getAnswerTotal;
      }
      var getPoints = await triviaAnswer.sum("point", {
        where: {
          member_id: idMember,
        },
      });
      var getPointFromPrize = await prizeRedeem.sum("redeem_point", {
        where: {
          member_id: idMember,
          result: 1,
        },
      });

      var poinFromSpin = await spinPlay.sum("point", {
        where: {
          member_id: idMember,
          type: "point",
        },
      });

      if (!getPointFromPrize && !poinFromSpin) {
        var pointTotals = getPoints;
      } else if (!poinFromSpin) {
        var pointTotals = getPoints - getPointFromPrize;
      } else if (!getPointFromPrize) {
        var pointTotals = getPoints + poinFromSpin;
      } else {
        var pointTotals = poinFromSpin + getPoints - getPointFromPrize;
      }

      const validTotalAnswer = await triviaAnswer.count({
        where: {
          member_id: idMember,
          is_correct: {
            [Op.not]: 2,
          },
        },
      });

      const correctedAnswers = await triviaAnswer.count({
        where: {
          member_id: idMember,
          is_correct: 1,
        },
      });

      const wrongAnswer = await triviaAnswer.count({
        where: {
          member_id: idMember,
          is_correct: 0,
        },
      });

      const response = {
        status: 200,
        success: true,
        message: "OK",
        data: {
          phoneNo: profile.msisdn,
          photo: imgUser,
          credits: creditTotals,
          points: pointTotals,
          totalAnswers: validTotalAnswer,
          correctedAnswers: correctedAnswers,
          wrongAnswers: wrongAnswer,
          unreadMessage: inboxUnread,
        },
      };
      return res.status(200).send(response);
    } catch (err) {
      const response = {
        status: 400,
        success: false,
        message: "Bad Request",
        data: {
          errors: err.message,
        },
      };
      return res.status(400).send(response);
    }
  },
  async getFile(req, res) {
    try {
      const fileName = req.params.name;
      const directoryPath = __basedir + "/uploads/profile/";
      res.sendFile(directoryPath + fileName, fileName);
    } catch (err) {
      const response = {
        status: 400,
        success: false,
        message: "Bad Request",
        data: {
          errors: err.message,
        },
      };
      return res.status(400).send(response);
    }
  },
};
