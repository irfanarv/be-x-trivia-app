const members = require("../../models").members;
const util = require("util");
const multer = require("multer");
const path = require("path");
const jwt = require("jsonwebtoken");
const secret = process.env.SECRET;
const maxSize = 2 * 1024 * 1024;
const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, path.join(__dirname, "../../../uploads/profile"));
  },
  filename: function (req, file, cb) {
    cb(
      null,
      file.fieldname + "_" + Date.now() + path.extname(file.originalname)
    );
  },
});

const uploadFile = multer({
  storage: storage,
  limits: { fileSize: maxSize },
}).single("image");

let uploadFileMiddleware = util.promisify(uploadFile);

module.exports = {
  async change(req, res) {
    try {
      await uploadFileMiddleware(req, res);
      if (req.file == undefined) {
        const response = {
          status: 400,
          success: false,
          message: "Pilih avatar baru terlebih dahulu!",
        };
        return res.status(400).send(response);
      } else {
        const usertoken = req.headers["x-access-token"];
        const token = usertoken.split(" ");
        const userData = jwt.verify(token[1], secret);
        const idMember = userData.member_id;
        await members
          .update(
            {
              photo: req.file.filename,
            },
            {
              where: {
                id: idMember,
              },
            }
          )
          .then((results) => {
            const response = {
              status: 200,
              success: true,
              message: "Berhasil memperbaharui profile",
            };
            return res.status(200).send(response);
          });
      }
    } catch (err) {
      if (err.code == "LIMIT_FILE_SIZE") {
        return res.status(400).send({
          message: "Ukuran file tidak boleh lebih dari 2 MB!",
        });
      }
      const response = {
        status: 400,
        success: false,
        message: "Bad Request",
        data: {
          errors: err.message,
        },
      };
      return res.status(400).send(response);
    }
  },
};
