const inbox = require("../../models").inboxes;
const jwt = require("jsonwebtoken");
const secret = process.env.SECRET;

module.exports = {
  async main(req, res) {
    try {
      const usertoken = req.headers["x-access-token"];
      const token = usertoken.split(" ");
      const userData = jwt.verify(token[1], secret);
      const idMember = userData.member_id;
      await inbox
        .findAll({
          where: {
            member_id: idMember,
          },
          order: [["createdAt", "DESC"]],
        })
        .then((results) => {
          const inboxList = [];
          results.forEach((results) => {
            inboxList.push({
              inboxID: results.id,
              title: results.title,
              message: results.text,
              time: results.createdAt,
            });
          });
          if (!inboxList.length) {
            const response = {
              status: 200,
              success: true,
              message: "Kosong",
            };
            return res.status(200).send(response);
          }
          const response = {
            status: 200,
            success: true,
            message: "OK",
            data: inboxList,
          };
          return res.status(200).send(response);
        });
    } catch (err) {
      const response = {
        status: 400,
        success: false,
        message: "Bad Request",
        data: {
          errors: err.message,
        },
      };
      return res.status(400).send(response);
    }
  },
};
