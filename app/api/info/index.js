const Info = require("../../models").infos;
const { Op } = require("sequelize");
module.exports = {
  async getTnC(req, res) {
    try {
      await Info.findOne({
        where: {
          type: {
            [Op.like]: "%" + "tnc" + "%",
          },
        },
      }).then((results) => {
        if (!results) {
          const response = {
            status: 400,
            success: true,
            message: "Data tidak ditemukan",
            data: results,
          };
          return res.status(400).send(response);
        }
        const response = {
          status: 200,
          success: true,
          message: "OK",
          data: results,
        };
        return res.status(200).send(response);
      });
    } catch (err) {
      const response = {
        status: 400,
        success: false,
        message: "Bad Request",
        data: {
          errors: err.message,
        },
      };
      return res.status(400).send(response);
    }
  },
  async getPrivacy(req, res) {
    try {
      await Info.findOne({
        where: {
          type: {
            [Op.like]: "%" + "privacy-policy" + "%",
          },
        },
      }).then((results) => {
        if (!results) {
          const response = {
            status: 400,
            success: true,
            message: "Data tidak ditemukan",
            data: results,
          };
          return res.status(400).send(response);
        }
        const response = {
          status: 200,
          success: true,
          message: "OK",
          data: results,
        };
        return res.status(200).send(response);
      });
    } catch (err) {
      const response = {
        status: 400,
        success: false,
        message: "Bad Request",
        data: {
          errors: err.message,
        },
      };
      return res.status(400).send(response);
    }
  },
  async getAbout(req, res) {
    try {
      await Info.findOne({
        where: {
          type: {
            [Op.like]: "%" + "about" + "%",
          },
        },
      }).then((results) => {
        if (!results) {
          const response = {
            status: 400,
            success: true,
            message: "Data tidak ditemukan",
            data: results,
          };
          return res.status(400).send(response);
        }
        const response = {
          status: 200,
          success: true,
          message: "OK",
          data: results,
        };
        return res.status(200).send(response);
      });
    } catch (err) {
      const response = {
        status: 400,
        success: false,
        message: "Bad Request",
        data: {
          errors: err.message,
        },
      };
      return res.status(400).send(response);
    }
  },
};
