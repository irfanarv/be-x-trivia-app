const subscribeHits = require("../../models").payment_subscribe_hits;
const subscribePackages = require("../../models").topup_subscribe_packages;
const dcbPackages = require("../../models").topup_dcb_packages;
const triviaAnswer = require("../../models").trivia_answers;
const prizeRedeem = require("../../models").prize_redeems;
const totalCredits = require("../../models").member_credits;
const totalSpins = require("../../models").member_spins;
const spinPlay = require("../../models").lucky_spin_plays;
const jwt = require("jsonwebtoken");
const secret = process.env.SECRET;

module.exports = {
  async main(req, res) {
    try {
      const usertoken = req.headers["x-access-token"];
      const token = usertoken.split(" ");
      const userData = jwt.verify(token[1], secret);
      const idMember = userData.member_id;
      var phoneNumber = userData.msisdn;
      // get credits
      const getCredits = await totalCredits.sum("amount", {
        where: {
          member_id: idMember,
        },
      });

      const getAnswerTotal = await triviaAnswer.count({
        where: {
          member_id: idMember,
        },
      });
      if (!getAnswerTotal) {
        var creditTotals = getCredits;
      } else {
        var creditTotals = getCredits - getAnswerTotal;
      }
      // end get credits
      // get point
      var getPoints = await triviaAnswer.sum("point", {
        where: {
          member_id: idMember,
        },
      });
      var getPointFromPrize = await prizeRedeem.sum("redeem_point", {
        where: {
          member_id: idMember,
          result: 1,
        },
      });
      var poinFromSpin = await spinPlay.sum("point", {
        where: {
          member_id: idMember,
          type: "point",
        },
      });

      if (!getPointFromPrize && !poinFromSpin) {
        var pointTotals = getPoints;
      } else if (!poinFromSpin) {
        var pointTotals = getPoints - getPointFromPrize;
      } else if (!getPointFromPrize) {
        var pointTotals = getPoints + poinFromSpin;
      } else {
        var pointTotals = poinFromSpin + getPoints - getPointFromPrize;
      }
      const getKuotaSpin = await totalSpins.sum("quota_spin", {
        where: {
          member_id: idMember,
        },
      });
      const getTotalPlaySpin = await spinPlay.count({
        where: {
          member_id: idMember,
        },
      });

      if (!getTotalPlaySpin) {
        var spinTotals = getKuotaSpin;
      } else {
        var spinTotals = getKuotaSpin - getTotalPlaySpin;
      }
      // end get point
      const subscribe = await subscribePackages.findAll({
        order: [["id", "ASC"]],
      });
      const subscribePackage = [];
      subscribe.forEach((subscribe) => {
        subscribePackage.push({
          telco: subscribe.telco,
          sdc: subscribe.sdc,
          keyword: subscribe.keyword,
          price: subscribe.price,
          spinBonus: subscribe.lucky_spin_bonus,
          imgBerlangganan:
            process.env.URL_FILE +
            "/topup/subscribe-packages/" +
            subscribe.image_langganan,
          imgBerhenti:
            process.env.URL_FILE +
            "/topup/subscribe-packages/" +
            subscribe.image_berhenti,
        });
      });

      await dcbPackages
        .findAll({
          order: [["id", "ASC"]],
        })
        .then((package) => {
          const dcbPackage = [];
          package.forEach((package) => {
            dcbPackage.push({
              creditAmount: package.amount,
              price: package.price,
              spinBonus: package.lucky_spin_bonus,
              imgPackage:
                process.env.URL_FILE + "/topup/dcb-packages/" + package.image,
            });
          });
          const response = {
            status: 200,
            success: true,
            message: "OK",
            data: {
              creditTotals,
              pointTotals,
              spinTotals,
              phoneNumber,
              packages: {
                dcbPackage,
                subscribePackage,
              },
            },
          };
          return res.status(200).send(response);
        });
    } catch (err) {
      const response = {
        status: 400,
        success: false,
        message: "Bad Request",
        data: {
          errors: err.message,
        },
      };
      return res.status(400).send(response);
    }
  },
};
