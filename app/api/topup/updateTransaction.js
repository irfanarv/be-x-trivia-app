const updateTrx = require("../../models").payment_transactions;
const dates = require("../../utils/dates.util");
const addToCredit = require("../../models").member_credits;
const addToSpin = require("../../models").member_spins;
const dcbPackages = require("../../models").topup_dcb_packages;
const jwt = require("jsonwebtoken");
const secret = process.env.SECRET;

module.exports = {
  async main(req, res) {
    try {
      let trxId = req.query.trx_id;
      let statusTrx = req.query.status;
      let orderTime = dates.getDateString(req.query.order_time);

      const usertoken = req.headers["x-access-token"];
      const token = usertoken.split(" ");
      const userData = jwt.verify(token[1], secret);
      const idMember = userData.member_id;
      var currentdate = new Date();

      if (statusTrx === "success") {
        var statusId = 1;
      } else {
        var statusId = 0;
      }
      var trxData = await updateTrx.findOne({
        where: {
          id: trxId,
        },
      });

      var validCreditAmount = await dcbPackages.findOne({
        where: {
          amount: trxData.amount,
        },
      });
      await updateTrx
        .update(
          {
            status: statusId,
            order_id: req.query.order_id,
            order_time: orderTime,
          },
          {
            where: {
              id: trxId,
            },
          }
        )
        .then(async (results) => {
          if (statusTrx === "success") {
            await addToCredit.create({
              member_id: idMember,
              date: currentdate,
              payment_id: trxId,
              amount: validCreditAmount.amount,
              type: "Topup Google Play",
            });
            if (validCreditAmount.lucky_spin_bonus > 0) {
              await addToSpin.create({
                member_id: idMember,
                payment_id: trxId,
                date: currentdate,
                quota_spin: validCreditAmount.lucky_spin_bonus,
                type: "Topup Google Play",
              });
            }
          }
          const response = {
            status: req.query.status,
          };
          return res.status(200).send(response);
        });
    } catch (err) {
      const response = {
        status: 400,
        success: false,
        message: "Bad Request",
        data: {
          errors: err.message,
        },
      };
      return res.status(400).send(response);
    }
  },
};
