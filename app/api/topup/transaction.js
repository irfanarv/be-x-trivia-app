const storeTrx = require("../../models").payment_transactions;
const dcbPackages = require("../../models").topup_dcb_packages;
const dates = require("../../utils/dates.util");
const db = require("../../models");
const requestIp = require("request-ip");

module.exports = {
  async main(req, res) {
    try {
      let trxId, date, clientIp;

      // generate id
      let zeroPad = (num, places) => String(num).padStart(places, "0");
      date = dates.generateIDbyDate();
      clientIp = requestIp.getClientIp(req);
      // var orderTime = new Date();
      let lastTrxId = await db.sequelize.query(
        "SELECT MAX(RIGHT(id,7)) AS kd_max FROM payment_transactions WHERE DATE(createdAt)=CURDATE()",
        {
          type: db.sequelize.QueryTypes.SELECT,
        }
      );

      // fetch dcb package
      var package = await dcbPackages.findAll({
        attributes: ["amount"],
      });
      // get value object
      const values = package.map(function (package) {
        return package.amount;
      });
      const value = parseInt(req.query.credit);
      // check if params = values in db
      const hasValue = values.some((e) => e === value);
      // console.log(hasValue);
      if (hasValue == false) {
        const response = {
          status: "failed",
          message: "Credit point tidak tersedia",
        };
        return res.status(400).send(response);
      }

      [trxId] = lastTrxId.map((id_trx) => id_trx.kd_max);
      if (trxId !== null) {
        lastTrxId.forEach((idTrx) => {
          const temp = parseInt(idTrx.kd_max) + parseInt(1);
          const trx_id = "TRX" + date + zeroPad(temp, 7);
          storeTrx
            .create({
              id: trx_id,
              msisdn: req.query.phone_number,
              amount: req.query.credit,
              payment_method: "Google",
              // order_time: orderTime,
              user_ip: clientIp,
            })
            .then((results) => {
              const response = {
                status: "success",
                trx_id: trx_id,
              };
              return res.status(200).send(response);
            });
          // console.log(trx_id);
        });
      } else {
        const trx_id = "TRX" + date + zeroPad(1, 7);
        storeTrx
          .create({
            id: trx_id,
            msisdn: req.query.phone_number,
            amount: req.query.credit,
            payment_method: "Google",
            // order_time: orderTime,
            user_ip: clientIp,
          })
          .then((results) => {
            const response = {
              status: "success",
              trx_id: trx_id,
            };
            return res.status(200).send(response);
          });
      }
      // console.log(trxId);
    } catch (err) {
      const response = {
        status: 400,
        success: false,
        message: "Bad Request",
        data: {
          errors: err.message,
        },
      };
      return res.status(400).send(response);
    }
  },
};
