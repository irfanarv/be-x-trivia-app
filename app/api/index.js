const signup = require("./member/signUp");
const validateOtp = require("./member/validateOTP");
const login = require("./member/login");
const promotions = require("./promotions");
const categoryTrivia = require("./trivia/categories");
const subCategoryTrivia = require("./trivia/subCategories");
const question = require("./trivia/questions");
const answer = require("./trivia/answers");
const prizes = require("./prizes");
const leaderboard = require("./leaderboard");
const profile = require("./member/profile");
const info = require("./info");
const referral = require("./referral");
const packageTopup = require("./topup/topupPackages");
const tutorial = require("./tutorial");
const unsubscribe = require("./member/unsubscribe");
const inbox = require("./inbox");
const readMsg = require("./inbox/read");
const spin = require("./spin");
const spinPlay = require("./spin/countPlay");
const dailyLogin = require("./dailyLogins");
const claimLogin = require("./dailyLogins/claim");
const updateAva = require("./member/updateAvatar");
const homeCheck = require("./home");
const transaction = require("./topup/transaction");
const updateTransaction = require("./topup/updateTransaction");

module.exports = {
  signup,
  validateOtp,
  login,
  promotions,
  categoryTrivia,
  subCategoryTrivia,
  question,
  answer,
  prizes,
  leaderboard,
  profile,
  info,
  referral,
  packageTopup,
  tutorial,
  unsubscribe,
  inbox,
  spin,
  spinPlay,
  dailyLogin,
  claimLogin,
  readMsg,
  updateAva,
  homeCheck,
  transaction,
  updateTransaction,
};
