const dailyLogin = require("../../models").daily_logins;
const memberDailyLogins = require("../../models").member_daily_logins;
const jwt = require("jsonwebtoken");
const secret = process.env.SECRET;
const moment = require("moment");

module.exports = {
  async main(req, res) {
    try {
      const usertoken = req.headers["x-access-token"];
      const token = usertoken.split(" ");
      const userData = jwt.verify(token[1], secret);
      const idMember = userData.member_id;
      const today = moment();
      const kemarin = moment().subtract(1, "day");
      const hariKemarin = kemarin.format("YYYY-MM-DD");
      const hariIni = today.format("YYYY-MM-DD");

      var cekLastLogin = await memberDailyLogins.findOne({
        where: {
          member_id: idMember,
        },
        include: [
          {
            model: dailyLogin,
            as: "daily_login",
            attributes: ["day"],
          },
        ],
        order: [["createdAt", "DESC"]],
      });
      if (!cekLastLogin) {
        var hadiah = await dailyLogin.findOne({
          where: {
            day: 1,
          },
        });
        var imageDaily = process.env.URL_FILE + "/daily/" + hadiah.image;
        const response = {
          statusDaily: 1,
          status: 200,
          success: true,
          message: "OK",
          data: {
            dailyID: hadiah.id,
            day: hadiah.day,
            credit: hadiah.credit,
            img: imageDaily,
          },
        };
        return res.status(200).send(response);
      } else if (cekLastLogin.date_login === hariKemarin) {
        if (cekLastLogin.daily_login.day === 1) {
          var hadiah = await dailyLogin.findOne({
            where: {
              day: 2,
            },
          });
        } else if (cekLastLogin.daily_login.day === 2) {
          var hadiah = await dailyLogin.findOne({
            where: {
              day: 3,
            },
          });
        } else if (cekLastLogin.daily_login.day === 3) {
          var hadiah = await dailyLogin.findOne({
            where: {
              day: 1,
            },
          });
        }
        var imageDaily = process.env.URL_FILE + "/daily/" + hadiah.image;
        const response = {
          statusDaily: 0,
          status: 200,
          success: true,
          message: "OK",
          data: {
            dailyID: hadiah.id,
            day: hadiah.day,
            credit: hadiah.credit,
            img: imageDaily,
          },
        };
        return res.status(200).send(response);
      } else if (cekLastLogin.date_login === hariIni) {
        const response = {
          statusDaily: 1,
          status: 400,
          success: true,
          message: "Sudah submit ! Kembali lagi besok",
        };
        return res.status(400).send(response);
      } else {
        var hadiah = await dailyLogin.findOne({
          where: {
            day: 1,
          },
        });
        var imageDaily = process.env.URL_FILE + "/daily/" + hadiah.image;
        const response = {
          statusDaily: 0,
          status: 200,
          success: true,
          message: "OK",
          data: {
            dailyID: hadiah.id,
            day: hadiah.day,
            credit: hadiah.credit,
            img: imageDaily,
          },
        };
        return res.status(200).send(response);
      }
    } catch (err) {
      const response = {
        status: 400,
        success: false,
        message: "Bad Request",
        data: {
          errors: err.message,
        },
      };
      return res.status(400).send(response);
    }
  },
};
