const Prizes = require("../../models").prizes;
const triviaAnswer = require("../../models").trivia_answers;
const prizeRedeem = require("../../models").prize_redeems;
const totalCredits = require("../../models").member_credits;
const totalSpins = require("../../models").member_spins;
const spinPlay = require("../../models").lucky_spin_plays;
const jwt = require("jsonwebtoken");
const inbox = require("../../models").inboxes;
const secret = process.env.SECRET;
const { Op } = require("sequelize");

module.exports = {
  async main(req, res) {
    try {
      const usertoken = req.headers["x-access-token"];
      const token = usertoken.split(" ");
      const userData = jwt.verify(token[1], secret);
      const idMember = userData.member_id;
      // get credits
      const getCredits = await totalCredits.sum("amount", {
        where: {
          member_id: idMember,
        },
      });

      const getAnswerTotal = await triviaAnswer.count({
        where: {
          member_id: idMember,
        },
      });
      if (!getAnswerTotal) {
        var creditTotals = getCredits;
      } else {
        var creditTotals = getCredits - getAnswerTotal;
      }

      // end get credits
      // get point
      var getPoints = await triviaAnswer.sum("point", {
        where: {
          member_id: idMember,
        },
      });
      var getPointFromPrize = await prizeRedeem.sum("redeem_point", {
        where: {
          member_id: idMember,
          result: 1,
        },
      });
      var poinFromSpin = await spinPlay.sum("point", {
        where: {
          member_id: idMember,
          type: "point",
        },
      });

      if (!getPointFromPrize && !poinFromSpin) {
        var pointTotals = getPoints;
      } else if (!poinFromSpin) {
        var pointTotals = getPoints - getPointFromPrize;
      } else if (!getPointFromPrize) {
        var pointTotals = getPoints + poinFromSpin;
      } else {
        var pointTotals = poinFromSpin + getPoints - getPointFromPrize;
      }
      // end get point
      const getKuotaSpin = await totalSpins.sum("quota_spin", {
        where: {
          member_id: idMember,
        },
      });
      const getTotalPlaySpin = await spinPlay.count({
        where: {
          member_id: idMember,
        },
      });

      if (!getTotalPlaySpin) {
        var spinTotals = getKuotaSpin;
      } else {
        var spinTotals = getKuotaSpin - getTotalPlaySpin;
      }

      await Prizes.findAll({
        order: [["redeem_point", "ASC"]],
      }).then((getPrizes) => {
        const prizesList = [];
        getPrizes.forEach((getPrizes) => {
          prizesList.push({
            prizeID: getPrizes.id,
            prizeName: getPrizes.name,
            prizeImage: process.env.URL_FILE + "/prizes/" + getPrizes.image,
            reddemPoint: getPrizes.redeem_point,
            winnerTotal: getPrizes.winner_total,
          });
        });
        if (!prizesList.length) {
          const response = {
            status: 200,
            success: true,
            message: "Opps, Hadih tidak tersedia",
            data: null,
          };
          return res.status(200).send(response);
        }
        const response = {
          status: 200,
          success: true,
          message: "Hadiah tersedia",
          data: {
            creditTotals,
            pointTotals,
            spinTotals,
            prizesList,
          },
        };
        return res.status(200).send(response);
      });
    } catch (err) {
      const response = {
        status: 400,
        success: false,
        message: "Bad Request",
        data: {
          errors: err.message,
        },
      };
      return res.status(400).send(response);
    }
  },

  async redeem(req, res) {
    try {
      const now = new Date();
      const usertoken = req.headers["x-access-token"];
      const token = usertoken.split(" ");
      const userData = jwt.verify(token[1], secret);
      const idMember = userData.member_id;

      // get point member
      var getPoints = await triviaAnswer.sum("point", {
        where: {
          member_id: idMember,
        },
      });
      var getPointFromPrize = await prizeRedeem.sum("redeem_point", {
        where: {
          member_id: idMember,
          result: 1,
        },
      });
      var poinFromSpin = await spinPlay.sum("point", {
        where: {
          member_id: idMember,
          type: "point",
        },
      });

      if (!getPointFromPrize && !poinFromSpin) {
        var pointTotals = getPoints;
      } else if (!poinFromSpin) {
        var pointTotals = getPoints - getPointFromPrize;
      } else if (!getPointFromPrize) {
        var pointTotals = getPoints + poinFromSpin;
      } else {
        var pointTotals = poinFromSpin + getPoints - getPointFromPrize;
      }
      // end get point member
      await Prizes.findOne({
        where: {
          id: req.body.prizeID,
        },
      }).then(async (hadiah) => {
        if (!hadiah) {
          const response = {
            status: 400,
            success: false,
            message: "Hadiah tidak tersedia",
          };
          return res.status(400).send(response);
        } else {
          var totalPrizes = await prizeRedeem.count({
            where: {
              prize_id: hadiah.id,
            },
          });
          const pointCukup = pointTotals >= hadiah.redeem_point;
          const pointKurang = pointTotals < hadiah.redeem_point;
          const limitCukup = hadiah.winner_total >= totalPrizes;
          const limitKurang = hadiah.winner_total < totalPrizes;
          const sisaPoint = pointTotals - hadiah.redeem_point;
          const msgSuccess =
            "Selamat! proses redeem hadiah " + hadiah.name + "telah berhasil";
          const msgLimitHabis =
            "Proses Redeem gagal! kuota pemenang telah habis";
          const msgPointKurang = "Proses Redeem gagal! point kamu belum cukup";
          const msgPointLimitKurang =
            "Proses Redeem gagal! kuota pemenang telah habis & point kamu belum cukup";
          if (pointCukup && limitCukup) {
            await prizeRedeem
              .create({
                prize_id: hadiah.id,
                member_id: idMember,
                prize_point: hadiah.redeem_point,
                redeem_point: hadiah.redeem_point,
                date: now,
                result: 1,
                message: msgSuccess,
              })
              .then(async (hasilRedeem) => {
                const textSuccess =
                  "Selamat! proses redeem hadiah " +
                  hadiah.name +
                  " berhasil, tim X Trivia akan menghubungi kamu";
                await inbox.create({
                  member_id: idMember,
                  title: msgSuccess,
                  text: textSuccess,
                });
                const response = {
                  status: 200,
                  success: true,
                  message: msgSuccess,
                  data: {
                    memberId: idMember,
                    prize: hadiah.name,
                    redeemPoint: hadiah.redeem_point,
                    currentPoint: sisaPoint,
                    redeemDate: now,
                  },
                };
                return res.status(200).send(response);
              });
          } else if (pointCukup && limitKurang) {
            await prizeRedeem
              .create({
                prize_id: hadiah.id,
                member_id: idMember,
                prize_point: hadiah.redeem_point,
                redeem_point: hadiah.redeem_point,
                date: now,
                result: 0,
                message: msgLimitHabis,
              })
              .then(async (hasilRedeem) => {
                const textSuccess =
                  "Proses reddem hadiah " +
                  hadiah.name +
                  " gagal, hadiah sudah habis";
                await inbox.create({
                  member_id: idMember,
                  title: msgLimitHabis,
                  text: textSuccess,
                });
                const response = {
                  status: 200,
                  success: false,
                  message: msgLimitHabis,
                };
                return res.status(200).send(response);
              });
          } else if (pointKurang && limitCukup) {
            await prizeRedeem
              .create({
                prize_id: hadiah.id,
                member_id: idMember,
                prize_point: hadiah.redeem_point,
                redeem_point: pointTotals,
                date: now,
                result: 0,
                message: msgPointKurang,
              })
              .then(async (hasilRedeem) => {
                const textSuccess =
                  "Proses reddem hadiah " +
                  hadiah.name +
                  " gagal, point kamu kurang";
                await inbox.create({
                  member_id: idMember,
                  title: msgPointKurang,
                  text: textSuccess,
                });
                const response = {
                  status: 200,
                  success: false,
                  message: msgPointKurang,
                };
                return res.status(200).send(response);
              });
          } else if (pointKurang && limitKurang) {
            await prizeRedeem
              .create({
                prize_id: hadiah.id,
                member_id: idMember,
                prize_point: hadiah.redeem_point,
                redeem_point: pointTotals,
                date: now,
                result: 0,
                message: msgPointLimitKurang,
              })
              .then(async (hasilRedeem) => {
                const textSuccess =
                  "Proses reddem hadiah " +
                  hadiah.name +
                  " gagal, point kamu belum cukup dan kuota hadiah sudah terlampaui";
                await inbox.create({
                  member_id: idMember,
                  title: msgPointLimitKurang,
                  text: textSuccess,
                });
                const response = {
                  status: 200,
                  success: false,
                  message: msgPointLimitKurang,
                };
                return res.status(200).send(response);
              });
          }
        }
      });
    } catch (err) {
      const response = {
        status: 400,
        success: false,
        message: "Bad Request",
        data: {
          errors: err.message,
        },
      };
      return res.status(400).send(response);
    }
  },
};
