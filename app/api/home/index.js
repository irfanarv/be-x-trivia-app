const tutorial = require("../../models").tutorials;
const categories = require("../../models").tutorial_categories;
const dailyLogin = require("../../models").daily_logins;
const memberDailyLogins = require("../../models").member_daily_logins;
const member = require("../../models").members;
const jwt = require("jsonwebtoken");
const secret = process.env.SECRET;
const moment = require("moment");
const today = moment();
const kemarin = moment().subtract(1, "day");
const hariKemarin = kemarin.format("YYYY-MM-DD");
const hariIni = today.format("YYYY-MM-DD");

module.exports = {
  async main(req, res) {
    try {
      const usertoken = req.headers["x-access-token"];
      const token = usertoken.split(" ");
      const userData = jwt.verify(token[1], secret);
      const idMember = userData.member_id;

      var cekLastLogin = await memberDailyLogins.findOne({
        where: {
          member_id: idMember,
        },
        include: [
          {
            model: dailyLogin,
            as: "daily_login",
            attributes: ["day"],
          },
        ],
        order: [["createdAt", "DESC"]],
      });

      var cekTutorial = await member.findOne({
        where: {
          id: idMember,
        },
      });

      var tutorSelesai = cekTutorial.home_tutorial === 1;
      var tutorBelum = cekTutorial.home_tutorial === 0;
      var terakhirLogin = cekLastLogin;

      if (tutorBelum && !terakhirLogin) {
        const response = {
          status: 200,
          success: true,
          data: {
            statusTutorial: 1,
            statusDaily: 1,
          },
        };
        return res.status(200).send(response);
      } else if (tutorSelesai && !terakhirLogin) {
        const response = {
          status: 200,
          success: true,
          data: {
            statusTutorial: 0,
            statusDaily: 1,
          },
        };
        return res.status(200).send(response);
      } else if (tutorSelesai && terakhirLogin === hariIni) {
        const response = {
          status: 200,
          success: true,
          data: {
            statusTutorial: 0,
            statusDaily: 0,
          },
        };
        return res.status(200).send(response);
      } else if (tutorBelum && terakhirLogin === hariIni) {
        const response = {
          status: 200,
          success: true,
          data: {
            statusTutorial: 1,
            statusDaily: 0,
          },
        };
        return res.status(200).send(response);
      } else if (tutorSelesai && terakhirLogin !== hariIni) {
        const response = {
          status: 200,
          success: true,
          data: {
            statusTutorial: 0,
            statusDaily: 1,
          },
        };
        return res.status(200).send(response);
      } else if (tutorBelum && terakhirLogin !== hariIni) {
        const response = {
          status: 200,
          success: true,
          data: {
            statusTutorial: 0,
            statusDaily: 1,
          },
        };
        return res.status(200).send(response);
      }
    } catch (err) {
      const response = {
        status: 400,
        success: false,
        message: "Bad Request",
        data: {
          errors: err.message,
        },
      };
      return res.status(400).send(response);
    }
  },
};
