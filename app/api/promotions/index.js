const Promotions = require("../../models").promotions;
var dates = require("../../utils/dates.util");
const { Op } = require("sequelize");

module.exports = {
  async main(req, res) {
    try {
      const dateNow = dates.currentDateOnly();
      var getPromo = await Promotions.findAll({
        where: {
          date_start: {
            [Op.lte]: dateNow,
          },
          date_end: {
            [Op.gte]: dateNow,
          },
        },
        order: [["createdAt", "ASC"]],
      });

      const promos = [];
      getPromo.forEach((getPromo) => {
        promos.push({
          promoName: getPromo.name,
          promoImage: process.env.URL_FILE + "/promotion/" + getPromo.image,
          promoHomeImage:
            process.env.URL_FILE + "/promotion/" + getPromo.image_home,
          promoAction: getPromo.action,
        });
      });
      if (!promos.length) {
        const response = {
          status: 200,
          success: true,
          promoStatus: null,
          message: "Promo tidak tersedia",
        };
        return res.status(200).send(response);
      } else {
        const response = {
          status: 200,
          success: true,
          promoStatus: true,
          message: "Promo tersedia",
          data: promos,
        };
        return res.status(200).send(response);
      }
    } catch (err) {
      const response = {
        status: 400,
        success: false,
        message: "Bad Request",
        data: {
          errors: err.message,
        },
      };
      return res.status(400).send(response);
    }
  },
};
