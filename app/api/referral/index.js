const member = require("../../models").members;
const jwt = require("jsonwebtoken");
const secret = process.env.SECRET;
module.exports = {
  async main(req, res) {
    try {
      const usertoken = req.headers["x-access-token"];
      const token = usertoken.split(" ");
      const userData = jwt.verify(token[1], secret);
      const idMember = userData.member_id;
      await member
        .findOne({
          attributes: ["referral_id"],
          where: {
            id: idMember,
          },
        })
        .then((results) => {
          const response = {
            status: 200,
            success: true,
            message: "OK",
            data: results,
          };
          return res.status(200).send(response);
        });
    } catch (err) {
      const response = {
        status: 400,
        success: false,
        message: "Bad Request",
        data: {
          errors: err.message,
        },
      };
      return res.status(400).send(response);
    }
  },
};
