"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class members extends Model {
    static associate(models) {}
  }
  members.init(
    {
      id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
        allowNull: false,
      },
      msisdn: DataTypes.STRING,
      referral_id: DataTypes.STRING,
      reg_referral_id: DataTypes.STRING,
      photo: DataTypes.STRING,
      status: DataTypes.INTEGER,
      home_tutorial: DataTypes.INTEGER,
      unsubscribe_date: DataTypes.DATE,
    },
    {
      sequelize,
      modelName: "members",
    }
  );
  return members;
};
