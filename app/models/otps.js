'use strict';
const {
    Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    class otp extends Model {

        static associate(models) {
            otp.belongsTo(models.members, {
                foreignKey: 'member_id',
                as: 'member'
            });
        }
    };
    otp.init({
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true,
            allowNull: false
        },
        member_id: DataTypes.INTEGER,
        otp: DataTypes.STRING,
        type: DataTypes.INTEGER,
        expiration_time: DataTypes.DATE,
    }, {
        sequelize,
        modelName: 'otp',
    });
    return otp;
};
