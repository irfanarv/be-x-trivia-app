'use strict';
const {
    Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    class trivia_categories extends Model {
        static associate(models) {
        }
    };
    trivia_categories.init({
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true,
            allowNull: false
        },
        name: DataTypes.STRING,
        image: DataTypes.STRING,
        image_active: DataTypes.STRING,
    }, {
        sequelize,
        modelName: 'trivia_categories',
    });
    return trivia_categories;
};
