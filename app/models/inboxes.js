"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class inboxes extends Model {
    static associate(models) {
      inboxes.belongsTo(models.members, {
        foreignKey: "member_id",
        as: "member",
      });
    }
  }
  inboxes.init(
    {
      id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
        allowNull: false,
      },
      title: DataTypes.STRING,
      text: DataTypes.STRING,
      read_status: DataTypes.INTEGER,
    },
    {
      sequelize,
      modelName: "inboxes",
    }
  );
  return inboxes;
};
