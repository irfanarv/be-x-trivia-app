"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class lucky_spin_plays extends Model {
    static associate(models) {
      lucky_spin_plays.belongsTo(models.members, {
        foreignKey: "member_id",
        as: "member",
      });
    }
  }
  lucky_spin_plays.init(
    {
      id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
        allowNull: false,
      },
      member_id: DataTypes.INTEGER,
      date: DataTypes.DATE,
      point: DataTypes.INTEGER,
      type: DataTypes.STRING,
    },
    {
      sequelize,
      modelName: "lucky_spin_plays",
    }
  );
  return lucky_spin_plays;
};
