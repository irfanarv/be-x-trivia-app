"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class member_spins extends Model {
    static associate(models) {
      member_spins.belongsTo(models.members, {
        foreignKey: "member_id",
        as: "member",
      });
    }
  }
  member_spins.init(
    {
      id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
        allowNull: false,
      },
      member_id: DataTypes.INTEGER,
      payment_id: DataTypes.STRING,
      date: DataTypes.DATE,
      quota_spin: DataTypes.INTEGER,
      type: DataTypes.STRING,
    },
    {
      sequelize,
      modelName: "member_spins",
    }
  );
  return member_spins;
};
