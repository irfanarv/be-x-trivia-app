"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class infos extends Model {
    static associate(models) {}
  }
  infos.init(
    {
      id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
        allowNull: false,
      },
      type: DataTypes.ENUM("tnc", "privacy-police", "about"),
      title: DataTypes.STRING,
      text: DataTypes.INTEGER,
    },
    {
      sequelize,
      modelName: "infos",
    }
  );
  return infos;
};
