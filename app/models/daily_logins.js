"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class daily_logins extends Model {
    static associate(models) {}
  }
  daily_logins.init(
    {
      id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
        allowNull: false,
      },
      day: DataTypes.INTEGER,
      credit: DataTypes.INTEGER,
      image: DataTypes.STRING,
      image_done: DataTypes.STRING,
    },
    {
      sequelize,
      modelName: "daily_logins",
    }
  );
  return daily_logins;
};
