'use strict';
const {
    Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    class topup_dcb_packages extends Model {

        static associate(models) {
        }
    };
    topup_dcb_packages.init({
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true,
            allowNull: false
        },
        amount: DataTypes.INTEGER,
        price: DataTypes.INTEGER,
        lucky_spin_bonus: DataTypes.INTEGER,
        image: DataTypes.STRING
        
    }, {
        sequelize,
        modelName: 'topup_dcb_packages',
    });
    return topup_dcb_packages;
};
