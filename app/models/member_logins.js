'use strict';
const {
    Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    class member_logins extends Model {

        static associate(models) {
            member_logins.belongsTo(models.members, {
                foreignKey: 'member_id',
                as: 'member'
            });
        }
    };
    member_logins.init({
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true,
            allowNull: false
        },
        member_id: DataTypes.INTEGER,
        otp: DataTypes.STRING,
    }, {
        sequelize,
        modelName: 'member_logins',
    });
    return member_logins;
};
