"use strict";
const { Model, INTEGER } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class trivia_answers extends Model {
    static associate(models) {
      trivia_answers.belongsTo(models.members, {
        foreignKey: "member_id",
        as: "member",
      });
      trivia_answers.belongsTo(models.trivia_questions, {
        foreignKey: "trivia_question_id",
        as: "trivia_questions",
      });
      trivia_answers.belongsTo(models.trivia_question_details, {
        foreignKey: "trivia_question_detail_id",
        as: "trivia_question_detail",
      });
    }
  }
  trivia_answers.init(
    {
      id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
        allowNull: false,
      },
      trivia_sub_category_id: INTEGER,
      member_id: INTEGER,
      is_correct: DataTypes.INTEGER,
      point: DataTypes.INTEGER,
    },
    {
      sequelize,
      modelName: "trivia_answers",
    }
  );
  return trivia_answers;
};
