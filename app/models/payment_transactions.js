"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class payment_transactions extends Model {
    static associate(models) {}
  }
  payment_transactions.init(
    {
      id: {
        type: DataTypes.STRING,
        primaryKey: true,
        autoIncrement: false,
        allowNull: false,
      },
      amount: DataTypes.INTEGER,
      order_id: DataTypes.STRING,
      order_time: DataTypes.DATE,
      msisdn: DataTypes.STRING,
      payment_method: DataTypes.STRING,
      user_ip: DataTypes.STRING,
      status: DataTypes.INTEGER,
    },
    {
      sequelize,
      modelName: "payment_transactions",
    }
  );
  return payment_transactions;
};
