"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class member_daily_logins extends Model {
    static associate(models) {
      member_daily_logins.belongsTo(models.members, {
        foreignKey: "member_id",
        as: "member",
      });
      member_daily_logins.belongsTo(models.daily_logins, {
        foreignKey: "daily_login_id",
        as: "daily_login",
      });
    }
  }
  member_daily_logins.init(
    {
      id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
        allowNull: false,
      },
      member_id: DataTypes.INTEGER,
      daily_login_id: DataTypes.INTEGER,
      date_login: DataTypes.DATE,
    },
    {
      sequelize,
      modelName: "member_daily_logins",
    }
  );
  return member_daily_logins;
};
