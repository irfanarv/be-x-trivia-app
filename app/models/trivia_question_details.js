'use strict';
const {
    Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    class trivia_question_details extends Model {
        static associate(models) {
            trivia_question_details.belongsTo(models.trivia_questions, {
                foreignKey: 'trivia_question_id',
                as: 'trivia_questions'
            });
        }
    };
    trivia_question_details.init({
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true,
            allowNull: false
        },
        answer_text: DataTypes.STRING,
        sort_no: DataTypes.INTEGER,
        is_correct: DataTypes.INTEGER,
    }, {
        sequelize,
        modelName: 'trivia_question_details',
    });
    return trivia_question_details;
};
