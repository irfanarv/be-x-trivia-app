"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class prize_redeems extends Model {
    static associate(models) {
      prize_redeems.belongsTo(models.members, {
        foreignKey: "member_id",
        as: "member",
      });
      prize_redeems.belongsTo(models.prizes, {
        foreignKey: "prize_id",
        as: "prize",
      });
    }
  }
  prize_redeems.init(
    {
      id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
        allowNull: false,
      },
      prize_id: DataTypes.INTEGER,
      member_id: DataTypes.INTEGER,
      prize_point: DataTypes.INTEGER,
      redeem_point: DataTypes.INTEGER,
      date: DataTypes.DATE,
      result: DataTypes.INTEGER,
      message: DataTypes.STRING,
    },
    {
      sequelize,
      modelName: "prize_redeems",
    }
  );
  return prize_redeems;
};
