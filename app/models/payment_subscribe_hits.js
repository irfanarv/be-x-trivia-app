"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class payment_subscribe_hits extends Model {
    static associate(models) {
      payment_subscribe_hits.belongsTo(models.members, {
        foreignKey: "member_id",
        as: "member",
      });
      payment_subscribe_hits.belongsTo(models.topup_subscribe_packages, {
        foreignKey: "topup_subscribe_package_id",
        as: "subscribe_package",
      });
    }
  }
  payment_subscribe_hits.init(
    {
      id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
        allowNull: false,
      },
      topup_subscribe_package_id: DataTypes.INTEGER,
      member_id: DataTypes.INTEGER,
    },
    {
      sequelize,
      modelName: "payment_subscribe_hits",
    }
  );
  return payment_subscribe_hits;
};
