"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class member_credits extends Model {
    static associate(models) {
      member_credits.belongsTo(models.members, {
        foreignKey: "member_id",
        as: "member",
      });
    }
  }
  member_credits.init(
    {
      id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
        allowNull: false,
      },
      member_id: DataTypes.INTEGER,
      payment_id: DataTypes.STRING,
      date: DataTypes.DATE,
      amount: DataTypes.INTEGER,
      type: DataTypes.STRING,
    },
    {
      sequelize,
      modelName: "member_credits",
    }
  );
  return member_credits;
};
