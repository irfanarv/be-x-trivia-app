'use strict';
const {
    Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    class trivia_questions extends Model {
        static associate(models) {
            trivia_questions.belongsTo(models.trivia_sub_categories, {
                foreignKey: 'trivia_sub_category_id',
                as: 'trivia_sub_categories'
            });
            trivia_questions.hasMany(models.trivia_question_details, {
                foreignKey: 'trivia_question_id',
                as: 'trivia_question_details'
            });
        }
    };
    trivia_questions.init({
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true,
            allowNull: false
        },
        title: DataTypes.STRING,
        image: DataTypes.STRING,
    }, {
        sequelize,
        modelName: 'trivia_questions',
    });
    return trivia_questions;
};
