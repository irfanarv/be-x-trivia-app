"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class lucky_spin_prizes extends Model {
    static associate(models) {}
  }
  lucky_spin_prizes.init(
    {
      id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
        allowNull: false,
      },
      sort_no: DataTypes.INTEGER,
      type: DataTypes.STRING,
      point: DataTypes.INTEGER,
      label: DataTypes.STRING,
      bg_color: DataTypes.STRING,
    },
    {
      sequelize,
      modelName: "lucky_spin_prizes",
    }
  );
  return lucky_spin_prizes;
};
