'use strict';
const {
    Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    class tutorials extends Model {
        static associate(models) {
            tutorials.belongsTo(models.tutorial_categories, {
                foreignKey: 'tutorial_category_id',
                as: 'tutorial_category'
            });
        }
    };
    tutorials.init({
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true,
            allowNull: false
        },
        tutorial_category_id: DataTypes.INTEGER,
        image: DataTypes.STRING,
        step:DataTypes.INTEGER
    }, {
        sequelize,
        modelName: 'tutorials',
    });
    return tutorials;
};
