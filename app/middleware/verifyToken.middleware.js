const jwt = require('jsonwebtoken');
const secret = process.env.SECRET

module.exports = function (req, res, next) {
    let tokenHeader = req.headers['x-access-token'];

    if (tokenHeader?.split(' ')[0] !== 'Bearer') {
        const response = {
            "auth": false,
            "status": 400,
            "success": false,
            "message": "Bad Request",
            "errors": "Incorrect token format"
        }
        return res.status(400).send(response)
    }

    let token = tokenHeader?.split(' ')[1];


    if (!token) {
        const response = {
            "auth": false,
            "status": 403,
            "success": false,
            "message": "Errors",
            "errors": "No token provided"
        }
        return res.status(403).send(response)
    }

    jwt.verify(token, secret, (err, decoded) => {
        if (err) {
            const response = {
                "auth": false,
                "status": 500,
                "success": false,
                "message": "Errors",
                "errors": err
            }
            return res.status(500).send(response)
        }
        req.userId = decoded.id;
        next();
    });

};
