module.exports = function (req, res, next) {

    if (!process.env.GOSMS_API || !process.env.GOSMS_USER || !process.env.GOSMS_PWD) {
        const response = {
            "status": 503,
            "message": "OTP cant be used",
        }
        return res.status(503).send(response)
    }
    next();

};
