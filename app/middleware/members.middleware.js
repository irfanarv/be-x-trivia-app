const Members = require("../models").members;
const memberLogins = require("../models").member_logins;
const paymentTransaction = require("../models").payment_transactions;
const jwt = require("jsonwebtoken");
const secret = process.env.SECRET;

module.exports.cekMemberParam = function (req, res, next) {
  try {
    Members.findOne({
      where: {
        msisdn: req.query.phone_number,
      },
    }).then((memberCheck) => {
      if (!memberCheck) {
        res.status(400).send({
          status: "failed",
          message: "Nomor telpon belum terdaftar",
        });
        return;
      }
      next();
    });
  } catch (err) {
    const response = {
      status: 400,
      success: false,
      message: "Bad Request",
      data: {
        errors: err.message,
      },
    };
    return res.status(400).send(response);
  }
};

module.exports.fieldTopupPhone = function (req, res, next) {
  try {
    const { phone_number } = req.query;
    if (!phone_number) {
      const response = {
        status: "failed",
        message: "No Telpon tidak boleh kosong",
      };
      return res.status(400).send(response);
    }
    next();
  } catch (err) {
    const response = {
      status: 400,
      success: false,
      message: "Bad Request",
      data: {
        errors: err.message,
      },
    };
    return res.status(400).send(response);
  }
};

module.exports.fieldTopupCredit = function (req, res, next) {
  try {
    const { credit } = req.query;
    if (!credit) {
      const response = {
        status: 400,
        success: false,
        message: "Credit tidak boleh kosong",
      };
      return res.status(400).send(response);
    }
    next();
  } catch (err) {
    const response = {
      status: 400,
      success: false,
      message: "Bad Request",
      data: {
        errors: err.message,
      },
    };
    return res.status(400).send(response);
  }
};

module.exports.memberLogin = function (req, res, next) {
  try {
    Members.findOne({
      where: {
        msisdn: req.body.msisdn,
      },
    }).then((memberCheck) => {
      if (!memberCheck) {
        res.status(400).send({
          status: 400,
          success: false,
          message: "Nomor telpon belum terdaftar",
        });
        return;
      }
      next();
    });
  } catch (err) {
    const response = {
      status: 400,
      success: false,
      message: "Bad Request",
      data: {
        errors: err.message,
      },
    };
    return res.status(400).send(response);
  }
};

module.exports.memberRegister = function (req, res, next) {
  try {
    Members.findOne({
      where: {
        msisdn: req.body.msisdn,
      },
    }).then((memberCheck) => {
      if (memberCheck) {
        res.status(400).send({
          status: 400,
          success: false,
          message: "Nomor telpon sudah terdaftar !",
        });
        return;
      }
      next();
    });
  } catch (err) {
    const response = {
      status: 400,
      success: false,
      message: "Bad Request",
      data: {
        errors: err.message,
      },
    };
    return res.status(400).send(response);
  }
};

module.exports.memberStatus = function (req, res, next) {
  try {
    Members.findOne({
      where: {
        msisdn: req.body.msisdn,
      },
    }).then((memberCheck) => {
      if (memberCheck.status === 0) {
        res.status(400).send({
          status: 400,
          success: false,
          message: "Error",
          data: {
            errors: "Akun ini diblokir",
            phoneNumber: req.body.msisdn,
          },
        });
        return;
      }
      next();
    });
  } catch (err) {
    const response = {
      status: 400,
      success: false,
      message: "Bad Request",
      data: {
        errors: err.message,
      },
    };
    return res.status(400).send(response);
  }
};

module.exports.memberUnsbuscribe = function (req, res, next) {
  try {
    const usertoken = req.headers["x-access-token"];
    const token = usertoken.split(" ");
    const userData = jwt.verify(token[1], secret);
    const idMember = userData.member_id;
    Members.findOne({
      where: {
        id: idMember,
      },
    }).then((memberCheck) => {
      if (memberCheck.status === 5) {
        res.status(200).send({
          status: 200,
          success: false,
          message: "Kamu sudah berhenti berlangganan",
        });
        return;
      }
      next();
    });
  } catch (err) {
    const response = {
      status: 400,
      success: false,
      message: "Bad Request",
      data: {
        errors: err.message,
      },
    };
    return res.status(400).send(response);
  }
};

module.exports.chekOTP = function (req, res, next) {
  try {
    memberLogins
      .findOne({
        where: {
          otp: req.body.otp,
        },
      })
      .then((otpCheck) => {
        if (!otpCheck) {
          res.status(400).send({
            status: 400,
            success: false,
            message: "Kode OTP Salah !",
          });
          return;
        }
        next();
      });
  } catch (err) {
    const response = {
      status: 400,
      success: false,
      message: "Bad Request",
      data: {
        errors: err.message,
      },
    };
    return res.status(400).send(response);
  }
};

module.exports.fieldOTP = function (req, res, next) {
  try {
    const { otp } = req.body;
    if (!otp) {
      const response = {
        status: 400,
        success: false,
        message: "Kode OTP wajib diisi !",
      };
      return res.status(400).send(response);
    }
    next();
  } catch (err) {
    const response = {
      status: 400,
      success: false,
      message: "Bad Request",
      data: {
        errors: err.message,
      },
    };
    return res.status(400).send(response);
  }
};

module.exports.fieldReferral = function (req, res, next) {
  try {
    const { reg_referral_id } = req.body;
    if (!reg_referral_id) {
      const response = {
        status: 400,
        success: false,
        message: "Kode Referral wajib diisi !",
      };
      return res.status(400).send(response);
    }
    next();
  } catch (err) {
    const response = {
      status: 400,
      success: false,
      message: "Bad Request",
      data: {
        errors: err.message,
      },
    };
    return res.status(400).send(response);
  }
};

module.exports.fieldPhone = function (req, res, next) {
  try {
    const { msisdn } = req.body;
    if (!msisdn) {
      const response = {
        status: 400,
        success: false,
        message: "Nomor telpon wajib diisi !",
      };
      return res.status(400).send(response);
    }
    next();
  } catch (err) {
    const response = {
      status: 400,
      success: false,
      message: "Bad Request",
      data: {
        errors: err.message,
      },
    };
    return res.status(400).send(response);
  }
};

module.exports.chekReferral = function (req, res) {
  try {
    Members.findOne({
      where: {
        referral_id: req.body.reg_referral_id,
      },
    }).then((referralCheck) => {
      if (referralCheck) {
        res.status(200).send({
          status: 200,
          success: true,
          message: "Kode Referral terdaftar !",
          data: {
            reg_referral_id: req.body.reg_referral_id,
          },
        });
        return;
      } else {
        res.status(400).send({
          status: 400,
          success: false,
          message: "Kode Referral tidak cocok !",
        });
        return;
      }
    });
  } catch (err) {
    const response = {
      status: 400,
      success: false,
      message: "Bad Request",
      data: {
        errors: err.message,
      },
    };
    return res.status(400).send(response);
  }
};

module.exports.checkMembers = function (req, res, next) {
  try {
    Members.findOne({
      where: {
        msisdn: req.body.msisdn,
      },
    }).then((members) => {
      if (members) {
        res.status(400).send({
          status: 400,
          success: false,
          message: "Kamu sudah terdaftar, silahkan login",
        });
        return;
      }
      next();
    });
  } catch (err) {
    const response = {
      status: 400,
      success: false,
      message: "Bad Request",
      data: {
        errors: err.message,
      },
    };
    return res.status(400).send(response);
  }
};
// update transaction
module.exports.fieldUpdateTrx = function (req, res, next) {
  try {
    const { trx_id, order_id, order_time, status } = req.query;
    if (!trx_id) {
      const response = {
        status: "failed",
        message: "Parameter ID Transaksi dibutuhkan",
      };
      return res.status(400).send(response);
    } else if (!order_id) {
      const response = {
        status: "failed",
        message: "Parameter order ID dari google dibutuhkan",
      };
      return res.status(400).send(response);
    } else if (!order_time) {
      const response = {
        status: "failed",
        message: "Parameter waktu order dari google dibutuhkan",
      };
      return res.status(400).send(response);
    } else if (!status) {
      const response = {
        status: "failed",
        message: "Parameter status dibutuhkan",
      };
      return res.status(400).send(response);
    }
    next();
  } catch (err) {
    const response = {
      status: 400,
      success: false,
      message: "Bad Request",
      data: {
        errors: err.message,
      },
    };
    return res.status(400).send(response);
  }
};
module.exports.checkTrxId = function (req, res, next) {
  try {
    paymentTransaction
      .findOne({
        where: {
          id: req.query.trx_id,
        },
      })
      .then((trxCheck) => {
        if (!trxCheck) {
          res.status(400).send({
            status: "failed",
            message: "ID Transaksi tidak ditemukan!",
          });
          return;
        } else if (trxCheck.status === 1) {
          res.status(400).send({
            status: "failed",
            message: "Transaksi ini sudah berhasil!",
          });
          return;
        }
        next();
      });
  } catch (err) {
    const response = {
      status: 400,
      success: false,
      message: "Bad Request",
      data: {
        errors: err.message,
      },
    };
    return res.status(400).send(response);
  }
};
