const subCategory = require("../models").trivia_sub_categories;
const triviaAnswer = require("../models").trivia_answers;
const totalCredits = require("../models").member_credits;
const inbox = require("../models").inboxes;
const totalSpins = require("../models").member_spins;
const spinPlay = require("../models").lucky_spin_plays;
const jwt = require("jsonwebtoken");
const secret = process.env.SECRET;

module.exports.subCategoryCheck = function (req, res, next) {
  try {
    subCategory
      .findOne({
        where: {
          id: req.query.sub_category_id,
        },
      })
      .then((subCategoryCheck) => {
        if (!subCategoryCheck) {
          res.status(400).send({
            status: 400,
            success: false,
            data: null,
            message: "Sub Kategori tidak ada!",
          });
          return;
        }
        next();
      });
  } catch (err) {
    const response = {
      status: 400,
      success: false,
      message: "Bad Request",
      data: {
        errors: err.message,
      },
    };
    return res.status(400).send(response);
  }
};

module.exports.checkAnswer = function (req, res, next) {
  try {
    var { questionID, answerID } = req.body;
    if (!questionID) {
      const response = {
        status: 400,
        success: false,
        message: "Tidak bisa menjawab pertanyaan yang tidak ada!",
      };
      return res.status(400).send(response);
    } else {
      const usertoken = req.headers["x-access-token"];
      const token = usertoken.split(" ");
      const userData = jwt.verify(token[1], secret);
      const idMember = userData.member_id;
      triviaAnswer
        .findOne({
          where: {
            trivia_question_id: questionID,
            member_id: idMember,
          },
        })
        .then((results) => {
          if (results) {
            res.status(400).send({
              status: 400,
              success: false,
              data: null,
              message: "Kamu sudah menjawab pertanyaan ini!",
            });
            return;
          }
          next();
        });
    }
  } catch (err) {
    const response = {
      status: 400,
      success: false,
      message: "Bad Request",
      data: {
        errors: err.message,
      },
    };
    return res.status(400).send(response);
  }
};

module.exports.checkCredits = async function (req, res, next) {
  try {
    const usertoken = req.headers["x-access-token"];
    const token = usertoken.split(" ");
    const userData = jwt.verify(token[1], secret);
    const idMember = userData.member_id;
    var getCredits = await totalCredits.sum("amount", {
      where: {
        member_id: idMember,
      },
    });

    var getAnswerTotal = await triviaAnswer.count({
      where: {
        member_id: idMember,
      },
    });
    if (!getAnswerTotal) {
      var creditTotals = getCredits;
    } else {
      var creditTotals = getCredits - getAnswerTotal;
    }
    if (creditTotals <= 0) {
      const textSuccess =
        "Opps! Kuota pertanyaan kamu sudah habis, segera lakukan topup";
      await inbox.create({
        member_id: idMember,
        title: "Kuota pertanyaan kamu sudah habis",
        text: textSuccess,
      });
      res.status(400).send({
        status: 400,
        success: false,
        data: null,
        message:
          "Opps! Kuota pertanyaan kamu sudah habis, segera lakukan topup",
      });
      return;
    }
    next();
  } catch (err) {
    const response = {
      status: 400,
      success: false,
      message: "Bad Request",
      data: {
        errors: err.message,
      },
    };
    return res.status(400).send(response);
  }
};

module.exports.spinKuota = async function (req, res, next) {
  try {
    const usertoken = req.headers["x-access-token"];
    const token = usertoken.split(" ");
    const userData = jwt.verify(token[1], secret);
    const idMember = userData.member_id;
    const cekKuota = await totalSpins.findOne({
      where: {
        member_id: idMember,
      },
    });
    if (!cekKuota) {
      const response = {
        status: 400,
        success: false,
        message: "Kamu tidak memiliki kuota spin, segera lakukan topup",
      };
      return res.status(400).send(response);
    } else {
      const getKuotaSpin = await totalSpins.sum("quota_spin", {
        where: {
          member_id: idMember,
        },
      });
      const getTotalPlaySpin = await spinPlay.count({
        where: {
          member_id: idMember,
        },
      });
      if (!getTotalPlaySpin) {
        var spinTotals = getKuotaSpin;
      } else {
        var spinTotals = getKuotaSpin - getTotalPlaySpin;
      }

      if (spinTotals <= 0) {
        const response = {
          status: 400,
          success: false,
          message: "Kuota spin kamu habis!, segera lakukan topup",
        };
        return res.status(400).send(response);
      }
      next();
    }
  } catch (err) {
    const response = {
      status: 400,
      success: false,
      message: "Bad Request",
      data: {
        errors: err.message,
      },
    };
    return res.status(400).send(response);
  }
};
